import amp.Amplet;
import com.ampex.amperabase.*;
import org.bouncycastle.jcajce.provider.digest.SHA3;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DummyBlock implements IBlockAPI {
    @Override
    public BigInteger getHeight() {
        return BigInteger.ZERO;
    }

    @Override
    public String getPrevID() {
        return null;
    }

    @Override
    public String getID() {
        return "testID";
    }

    @Override
    public String getSolver() {
        return Utils.toBase64(new byte[]{1,2,3,4});
    }

    @Override
    public Long getTimestamp() {
        return System.currentTimeMillis();
    }

    @Override
    public byte[] getPayload() {
        return new byte[0];
    }

    @Override
    public String getMerkleRoot() {
        return null;
    }

    @Override
    public byte[] gpuHeader() {
        return new byte[0];
    }

    @Override
    public String header() {
        return "test";
    }

    private static byte[] hashBlockHeader(byte[] _header)
    {
        SHA3.DigestSHA3 md = new SHA3.Digest512();
        md.update(_header);
        return md.digest();
    }

    @Override
    public ITransAPI getCoinbase() {
        return new ITransAPI() {
            @Override
            public String getID() {
                return null;
            }

            @Override
            public List<IOutput> getOutputs() {
                List<IOutput> outs = new ArrayList<>();
                outs.add(new IOutput() {
                    @Override
                    public byte getVersion() {
                        return 0;
                    }

                    @Override
                    public byte[] serializeToBytes() {
                        return new byte[0];
                    }

                    @Override
                    public IAddress getAddress() {
                        return new IAddress() {
                            @Override
                            public String encodeForChain() {
                                return DummyKi.ENCODED_MAIN_ADDRESS;
                            }

                            @Override
                            public byte getVersion() {
                                return 0;
                            }

                            @Override
                            public String getID() {
                                return null;
                            }

                            @Override
                            public String getChecksum() {
                                return null;
                            }

                            @Override
                            public boolean isValid() {
                                return false;
                            }

                            @Override
                            public byte[] toByteArray() {
                                return new byte[0];
                            }

                            @Override
                            public boolean hasPrefix() {
                                return false;
                            }

                            @Override
                            public String getPrefix() {
                                return null;
                            }

                            @Override
                            public boolean isP2SH() {
                                return false;
                            }

                            @Override
                            public KeyType getKeyType() {
                                return null;
                            }

                            @Override
                            public AddressLength getAddressLength() {
                                return null;
                            }

                            @Override
                            public boolean canSpend(String s, String s1, boolean b, KeyType keyType) {
                                return false;
                            }

                            @Override
                            public String getChecksumGen() {
                                return null;
                            }

                            @Override
                            public boolean canSpendPrefixed(String s, String s1, String s2, boolean b, KeyType keyType) {
                                return false;
                            }
                        };
                    }

                    @Override
                    public BigInteger getAmount() {
                        return null;
                    }

                    @Override
                    public Token getToken() {
                        return null;
                    }

                    @Override
                    public int getIndex() {
                        return 0;
                    }

                    @Override
                    public String toJSON() {
                        return null;
                    }

                    @Override
                    public String getID() {
                        return null;
                    }

                    @Override
                    public long getTimestamp() {
                        return 0;
                    }
                });
                return outs;
            }

            @Override
            public List<IInput> getInputs() {
                return null;
            }

            @Override
            public BigInteger getFee() {
                return null;
            }

            @Override
            public void makeChange(BigInteger bigInteger, IAddress iAddress) {

            }

            @Override
            public byte[] toSignBytes() {
                return new byte[0];
            }

            @Override
            public void addSig(String s, String s1) {

            }

            @Override
            public String getSig(String s) {
                return null;
            }

            @Override
            public String getMessage() {
                return null;
            }

            @Override
            public boolean verifyCanSpend() {
                return false;
            }

            @Override
            public boolean verifyInputToOutput() {
                return false;
            }

            @Override
            public boolean verifySpecial(IKiAPI iKiAPI) {
                return false;
            }

            @Override
            public boolean verifySigs() {
                return false;
            }

            @Override
            public Amplet serializeToAmplet() {
                return null;
            }
        };
    }

    @Override
    public ITransAPI getTransaction(String s) {
        return null;
    }

    @Override
    public Set<String> getTransactionKeys() {
        return new HashSet<>();
    }

    @Override
    public String merkleRoot() {
        return null;
    }

    @Override
    public void addTransaction(ITransAPI iTransAPI) {

    }

    @Override
    public Amplet serializeToAmplet() {
        return null;
    }
}
