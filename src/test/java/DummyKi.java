import amp.Amplet;
import com.ampex.amperabase.*;
import com.ampex.amperabase.data.BaseValueWatchable;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class DummyKi implements IKiAPI {

    public static final String ENCODED_MAIN_ADDRESS = "testEncode";
    @Override
    public IChainManAPI getChainMan() {
        return new IChainManAPI() {
            @Override
            public BaseValueWatchable<BigInteger> currentHeight() {
                return null;
            }

            @Override
            public IBlockAPI getByHeight(BigInteger bigInteger) {
                return new DummyBlock();
            }

            @Override
            public BaseValueWatchable<BigInteger> getCurrentDifficulty() {
                return null;
            }

            @Override
            public short getChainVer() {
                return 0;
            }

            @Override
            public BlockState addBlock(IBlockAPI iBlockAPI) {
                return null;
            }

            @Override
            public BlockState softVerifyBlock(IBlockAPI iBlockAPI) {
                return null;
            }

            @Override
            public BlockState verifyBlock(IBlockAPI iBlockAPI) {
                return null;
            }

            @Override
            public void setDifficulty(BigInteger bigInteger) {

            }

            @Override
            public IBlockAPI getTemp() {
                return null;
            }

            @Override
            public IBlockAPI formBlock(BigInteger bigInteger, String s, String s1, byte[] bytes, String s2, String s3, long l, byte[] bytes1) {
                return null;
            }

            @Override
            public IBlockAPI formBlock(Amplet amplet) {
                return null;
            }
        };
    }

    @Override
    public ITransManAPI getTransMan() {
        return null;
    }

    @Override
    public IEncryptManAPI getEncryptMan() {
        return null;
    }

    @Override
    public IAddManAPI getAddMan() {
        return new IAddManAPI() {
            @Override
            public IAddress getNewAdd(KeyType keyType, boolean b) {
                return null;
            }

            @Override
            public IAddress getMainAdd() {
                return new IAddress() {
                    @Override
                    public String encodeForChain() {
                        return ENCODED_MAIN_ADDRESS;
                    }

                    @Override
                    public byte getVersion() {
                        return 0;
                    }

                    @Override
                    public String getID() {
                        return null;
                    }

                    @Override
                    public String getChecksum() {
                        return null;
                    }

                    @Override
                    public boolean isValid() {
                        return false;
                    }

                    @Override
                    public byte[] toByteArray() {
                        return new byte[0];
                    }

                    @Override
                    public boolean hasPrefix() {
                        return false;
                    }

                    @Override
                    public String getPrefix() {
                        return null;
                    }

                    @Override
                    public boolean isP2SH() {
                        return false;
                    }

                    @Override
                    public KeyType getKeyType() {
                        return null;
                    }

                    @Override
                    public AddressLength getAddressLength() {
                        return null;
                    }

                    @Override
                    public boolean canSpend(String s, String s1, boolean b, KeyType keyType) {
                        return false;
                    }

                    @Override
                    public String getChecksumGen() {
                        return null;
                    }

                    @Override
                    public boolean canSpendPrefixed(String s, String s1, String s2, boolean b, KeyType keyType) {
                        return false;
                    }
                };
            }

            @Override
            public String getEntropyForAdd(IAddress iAddress) {
                return null;
            }

            @Override
            public IAddress createNew(String s, String s1, String s2, AddressLength addressLength, boolean b, KeyType keyType) {
                return null;
            }

            @Override
            public List<IAddress> getAll() {
                List<IAddress> all = new ArrayList<>();
                all.add( new IAddress() {
                    @Override
                    public String encodeForChain() {
                        return ENCODED_MAIN_ADDRESS;
                    }

                    @Override
                    public byte getVersion() {
                        return 0;
                    }

                    @Override
                    public String getID() {
                        return null;
                    }

                    @Override
                    public String getChecksum() {
                        return null;
                    }

                    @Override
                    public boolean isValid() {
                        return false;
                    }

                    @Override
                    public byte[] toByteArray() {
                        return new byte[0];
                    }

                    @Override
                    public boolean hasPrefix() {
                        return false;
                    }

                    @Override
                    public String getPrefix() {
                        return null;
                    }

                    @Override
                    public boolean isP2SH() {
                        return false;
                    }

                    @Override
                    public KeyType getKeyType() {
                        return null;
                    }

                    @Override
                    public AddressLength getAddressLength() {
                        return null;
                    }

                    @Override
                    public boolean canSpend(String s, String s1, boolean b, KeyType keyType) {
                        return false;
                    }

                    @Override
                    public String getChecksumGen() {
                        return null;
                    }

                    @Override
                    public boolean canSpendPrefixed(String s, String s1, String s2, boolean b, KeyType keyType) {
                        return false;
                    }});
                return all;
            }

            @Override
            public IAddress createFromByteArray(byte[] bytes) {
                return DummyAddress.get();
            }

            @Override
            public IAddress decodeFromChain(String s) {
                return null;
            }
        };
    }

    @Override
    public String getVersion() {
        return null;
    }

    @Override
    public void debug(String s) {

    }

    @Override
    public Options getOptions() {
        return null;
    }

    @Override
    public INetworkManagerAPI getNetMan() {
        return null;
    }

    @Override
    public void setStartHeight(BigInteger bigInteger) {

    }

    @Override
    public GUIHook getGUIHook() {
        return null;
    }

    @Override
    public void downloadedTo(BigInteger bigInteger) {

    }

    @Override
    public void blockTick(IBlockAPI iBlockAPI) {

    }

    @Override
    public IStateManager getStateManager() {
        return null;
    }

    @Override
    public void doneDownloading() {

    }

    @Override
    public void newTransPool() {

    }

    @Override
    public IAddressBook getAddressBook() {
        return null;
    }

    @Override
    public INetworkManagerAPI getPoolNet() {
        return null;
    }

    @Override
    public PoolData getPoolData() {
        return null;
    }

    @Override
    public String getStringSetting(StringSettings stringSettings) {
        return null;
    }

    @Override
    public void setStringSetting(StringSettings stringSettings, String s) {

    }

    @Override
    public boolean getSetting(Settings settings) {
        return false;
    }

    @Override
    public void setSetting(Settings settings, boolean b) {

    }
}
