import com.ampex.amperabase.*;
import logging.IAmpexLogger;
import mining_pool.IPool;
import mining_pool.Pool;
import mining_pool.PoolLogging;
import mining_pool.database.data.IPaymentReceipt;
import mining_pool.events.IPoolEventHandler;
import mining_pool.events.PoolEventType;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class PPLNSTest {

    @Test
    public void testPPLNS()
    {
        Pool pool = new Pool(new BigInteger("-1"), new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16), 0, new DummyKi(), new IPoolEventHandler() {
            @Override
            public void handle(IPool iPool, PoolEventType poolEventType) throws Exception {

            }
        });
        pool.start();
//        PoolLogging.startLogging(new IAmpexLogger() {
//            @Override
//            public void trace(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void info(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void debug(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void warn(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void warn(String s, Throwable throwable) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void warn(Throwable throwable) {
//                System.out.println(throwable.getCause().getMessage());
//            }
//
//            @Override
//            public void error(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void error(String s, Throwable throwable) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void error(Throwable throwable) {
//                System.out.println(throwable.getCause().getMessage());
//            }
//
//            @Override
//            public void fatal(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void fatal(String s, Throwable throwable) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void fatal(Throwable throwable) {
//                System.out.println(throwable.getCause().getMessage());
//            }
//        });
        List<IBlockAPI> blocks = new ArrayList<>();
        blocks.add(new DummyBlock());
        pool.addPPLNSShare(new DummyBlock());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals("PPLNS shares not found",1, pool.getTotalPPLNSSharesOfMiner(DummyAddress.get()));
        pool.endPPLNSRound(blocks,0.01,new DummyKi());

        Assert.assertTrue("Payment receipts not found",pool.areReceiptsAvailable());
        IPaymentReceipt receipt = pool.popReceipt();
        Assert.assertNotNull(receipt);
        Assert.assertNotNull(receipt.getPayee());
        Assert.assertNotNull(receipt.getPayee().getIDAsString());

        Assert.assertEquals("Payment address incorrect",receipt.getPayee().getIDAsString(),DummyAddress.get().encodeForChain());
        Assert.assertEquals(receipt.getOriginToPay(), 40_000_000_000_00000L - (40_000_000_000_00000L * 0.01),1D);

        pool.interrupt();
    }
}
