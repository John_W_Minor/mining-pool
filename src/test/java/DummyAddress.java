import com.ampex.amperabase.AddressLength;
import com.ampex.amperabase.IAddress;
import com.ampex.amperabase.KeyType;
import com.ampex.amperabase.Utils;

public class DummyAddress  implements IAddress {

    public static DummyAddress get()
    {
        return new DummyAddress();
    }
    @Override
    public String encodeForChain() {
        return Utils.toBase64(new byte[]{1,2,3,4});
    }

    @Override
    public byte getVersion() {
        return 0;
    }

    @Override
    public String getID() {
        return null;
    }

    @Override
    public String getChecksum() {
        return null;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public byte[] toByteArray() {
        return new byte[]{1,2,3,4};
    }

    @Override
    public boolean hasPrefix() {
        return false;
    }

    @Override
    public String getPrefix() {
        return null;
    }

    @Override
    public boolean isP2SH() {
        return false;
    }

    @Override
    public KeyType getKeyType() {
        return null;
    }

    @Override
    public AddressLength getAddressLength() {
        return null;
    }

    @Override
    public boolean canSpend(String s, String s1, boolean b, KeyType keyType) {
        return false;
    }

    @Override
    public String getChecksumGen() {
        return null;
    }

    @Override
    public boolean canSpendPrefixed(String s, String s1, String s2, boolean b, KeyType keyType) {
        return false;
    }
}
