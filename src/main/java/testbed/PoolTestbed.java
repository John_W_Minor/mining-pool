package testbed;

/**
 * Mining Pool library created by John Minor.
 */
public class PoolTestbed
{
    public static void main(String[] args)
    {
    }
    /*

    COMMENTED ENTIRE TESTBED SINCE IT WILL HAVE TO BE REWRITTEN
    private static final String SOLVER_ID = "my name is bob";

    public static void main(String[] args)
    {
        System.setProperty("log4j.configurationFile", "poollog4j.xml");

        AmpLogging.startLogging();
        PoolLogging.startLogging();

        IAddress address = Address.decodeFromChain("12501010bKEieAsY60D8RJR0NFivUb/WGJ0IMGw0GALPJA7VcN8=O6E+5uc5");

        byte[] bytesbytes = address.toByteArray();

        address = Address.fromByteArray(bytesbytes);

        MinerID minerID = MinerID.create(bytesbytes);

        boolean success = address.isValid();

        //long bbb = Long.MAX_VALUE;

        ArrayList<PPLNSSharesPerMiner> sharesPerMinerObjects = new ArrayList<>();

        MinerID temperoo = MinerID.create("1276+oWEWQJewLxWfa63Y/jDPCjB7zqbkt6/4ZHcw==5116".getBytes(AmpConstants.UTF8));

        for (int i = 0; i < 10; i++)
        {
            sharesPerMinerObjects.add(PPLNSSharesPerMiner.create(temperoo, 150));
        }

        ArrayList<BlockID> blockIDs = new ArrayList<>();

        for (int i = 0; i < 10; i++)
        {
            byte[] bytes = new byte[new Random().nextInt(100) + 1];
            new Random().nextBytes(bytes);

            BlockID tempBlockID = BlockID.create(bytes, BigInteger.TEN);

            byte[] tbibytes = tempBlockID.serializeToAmplet().serializeToBytes();

            BlockID tempestBlockID = BlockID.deserializeFromAmplet(Amplet.create(tbibytes));

            blockIDs.add(tempestBlockID);
        }

        PPLNSRoundShareLog log1 = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, 1, .01);

        sharesPerMinerObjects.add(PPLNSSharesPerMiner.create(temperoo, 150));
        PPLNSRoundShareLog log2 = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, 1, .01);

        sharesPerMinerObjects.add(PPLNSSharesPerMiner.create(temperoo, 150));
        PPLNSRoundShareLog log3 = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, 1, .01);

        sharesPerMinerObjects.add(PPLNSSharesPerMiner.create(temperoo, 150));
        PPLNSRoundShareLog log4 = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, 1, .01);

        sharesPerMinerObjects.add(PPLNSSharesPerMiner.create(temperoo, 150));
        PPLNSRoundShareLog log5 = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, 1, .01);

        ArrayList<PPLNSRoundShareLog> logs = new ArrayList<>();

        logs.add(log1);
        logs.add(log2);
        logs.add(log3);
        logs.add(log4);
        logs.add(log5);

        Amplet dingyAmp = PPLNSRoundShareLog.serializeMultipleToAmplet(logs);
        ArrayList<PPLNSRoundShareLog> logs2 = PPLNSRoundShareLog.deserializeMultipleFromAmplet(dingyAmp);

        int a = 0;


        byte[] diff = new byte[64];
        for (int i = 0; i < 64; i++)
        {
            diff[i] = -1;
        }
        diff[0] = 127;

        BigInteger shareDiff = new BigInteger(diff);

        System.out.println(shareDiff.toString(10));

        Pool pool = new Pool(new BigInteger("5", 10), shareDiff, 1, null);

        DefaultPoolEventHandler dpeh = new DefaultPoolEventHandler();

        try
        {
            dpeh.handle(pool, PoolEventType.RECEIPTS_AVAILABLE);
        } catch (Exception e)
        {

        }

        pool.registerEventHandler(dpeh);

        ArrayList<MinerID> miners = new ArrayList<>();

        for (int i = 0; i < 10_000; i++)
        {
            byte[] bytes = new byte[47];

            for (int j = 0; j < bytes.length; j++)
            {
                bytes[j] = (byte) (new Random().nextInt(26) + 65);
            }

            MinerID tempMinerID = MinerID.create(bytes);

            miners.add(tempMinerID);
        }

        pool.start();

        Thread simulatedClientThread = new Thread()
        {
            public void run()
            {
                int i = 0;

                while (true)
                {
                    try
                    {
                        Thread.sleep(1);
                    } catch (Exception e)
                    {

                    }

                    for (int j = 0; j < 100; j++)
                    {
                        MinerID tempMinerID = miners.get(i);
                        pool.addPPLNSShare(makeBlock(tempMinerID));
                        i++;

                        if (i == miners.size())
                        {
                            i = 0;
                        }
                    }

                    while (pool.areReceiptsAvailable())
                    {
                        PaymentReceipt tempReceipt = pool.popReceipt();

                        if (tempReceipt != null)
                        {
                            System.out.println(tempReceipt.getPayee().getIDAsString() + " is owed " + tempReceipt.getOriginToPay() + " Origin.");
                        }
                    }
                }
            }
        };

        simulatedClientThread.start();


        Thread simulatedPPLNSRoundEnder = new Thread()
        {
            public void run()
            {
                while (true)
                {
                    try
                    {
                        Thread.sleep(5000);
                    } catch (Exception e)
                    {

                    }
                    //PoolLogging.startLogging();
                    MinerID tempMinerID = miners.get(0);
                    ArrayList<Block> tempList = new ArrayList<>();
                    tempList.add(makeBlock(tempMinerID));
                    pool.endPPLNSRound(tempList, .1, null);
                    //PoolLogging.stopLogging();
                }
            }
        };

        simulatedPPLNSRoundEnder.start();
    }

    public static IBlockAPI makeBlock(MinerID _minerID)
    {
        byte[] bytes = new byte[512];
        //byte[] bytes = new byte[64];

        for (int i = 0; i < bytes.length; i++)
        {
            bytes[i] = (byte) (new Random().nextInt(26) + 65);
        }

        Transaction coinba = null;
        try
        {
            coinba = new Transaction(new String(bytes, AmpConstants.UTF8), 1, new HashMap<String, String>(), new ArrayList<Output>(), new ArrayList<Input>(), new HashMap<String, String>(), new ArrayList<String>(), TransactionType.STANDARD);
        } catch (InvalidTransactionException e)
        {
            e.printStackTrace();
        }
        IBlockAPI blork = new Block();
        blork.payload = _minerID.serializeToBytes();
        blork.timestamp = System.currentTimeMillis();
        blork.height = new BigInteger("6", 10);
        blork.prevID = new String(bytes, AmpConstants.UTF8);
        blork.ID = new String(bytes, AmpConstants.UTF8);
        blork.solver = SOLVER_ID;
        blork.setCoinbase(coinba);

        return blork;
    }

    */
}
