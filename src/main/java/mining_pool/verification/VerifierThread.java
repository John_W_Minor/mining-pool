package mining_pool.verification;

import mining_pool.verification.data.UnverifiedShare;
import mining_pool.verification.data.VerifiedShare;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Queue on 1/22/2018.
 */
public class VerifierThread implements Runnable
{

    private volatile ArrayList<UnverifiedShare> unverifiedShares = new ArrayList<>();

    private volatile ArrayList<VerifiedShare> verifiedShares = new ArrayList<>();

    private String name = "verifier";

    private Thread thrd = null;

    public VerifierThread(String _name)
    {
        name = _name;
    }


    public void addUnverifiedShare(UnverifiedShare _unverifiedShare)
    {
        if (_unverifiedShare != null)
        {
            unverifiedShares.add(_unverifiedShare);
        }
    }

    private void evaluateShares()
    {
        Iterator<UnverifiedShare> normalUnverifiedSharesIterator = unverifiedShares.iterator();
        while (normalUnverifiedSharesIterator.hasNext())
        {
            UnverifiedShare tempUnverifiedShare = normalUnverifiedSharesIterator.next();
            verifiedShares.add(tempUnverifiedShare.verify());
            normalUnverifiedSharesIterator.remove();
        }
    }

    public ArrayList<VerifiedShare> getVerifiedShares()
    {
        ArrayList<VerifiedShare> tempArrayList = verifiedShares;
        verifiedShares = new ArrayList<>();
        return tempArrayList;
    }

    public void start()
    {
        thrd = new Thread(this, name);
        thrd.start();
    }

    public void run()
    {
        evaluateShares();
    }

    public void join()
    {
        while(true)
        {
            try
            {
                thrd.join();
                thrd = null;
                break;
            } catch (Exception e)
            {

            }
        }
    }
}
