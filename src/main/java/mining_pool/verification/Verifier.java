package mining_pool.verification;

import mining_pool.verification.data.UnverifiedShare;
import mining_pool.verification.data.UsedFullyVerifiedShares;
import mining_pool.verification.data.VerifiedShare;

import java.util.ArrayList;

/**
 * Created by Queue on 1/22/2018.
 */
public class Verifier
{
    private int numThreads = 0;
    private volatile ArrayList<UnverifiedShare> unverifiedShares = new ArrayList<>();
    private VerifierThread[] verifierThreads = null;


    private UsedFullyVerifiedShares usedFullyVerifiedShares = new UsedFullyVerifiedShares();

    private long count = 0;

    public Verifier()
    {
        numThreads = Runtime.getRuntime().availableProcessors() * 2;
        verifierThreads = new VerifierThread[numThreads];

        for (int i = 0; i < numThreads; i++)
        {
            verifierThreads[i] = new VerifierThread("Verifier Thread #" + i);
        }
    }

    public synchronized void addUnverifiedShare(UnverifiedShare _unverifiedShare)
    {
        unverifiedShares.add(_unverifiedShare);
    }

    public synchronized ArrayList<VerifiedShare> verify()
    {
        distributeUnverifiedShares();

        for (VerifierThread verifierThread : verifierThreads)
        {
            verifierThread.start();
        }

        count = 0;

        for(VerifierThread verifierThread : verifierThreads)
        {
            verifierThread.join();
        }

        ArrayList<VerifiedShare> halfVerifiedShares = new ArrayList<>();

        for (VerifierThread verifierThread : verifierThreads)
        {
            halfVerifiedShares.addAll(verifierThread.getVerifiedShares());
        }

        ArrayList<VerifiedShare> fullyVerifiedShares = new ArrayList<>();

        for (VerifiedShare halfVerifiedShare : halfVerifiedShares)
        {
            if (usedFullyVerifiedShares.addVerifiedShare(halfVerifiedShare))
            {
                fullyVerifiedShares.add(halfVerifiedShare);
            }
        }

        usedFullyVerifiedShares.prune();

        return fullyVerifiedShares;
    }

    private void distributeUnverifiedShares()
    {
        for(UnverifiedShare unverifiedShare: unverifiedShares)
        {
            int index = (int) (count % numThreads);

            verifierThreads[index].addUnverifiedShare(unverifiedShare);

            count++;

            if (count < 0)
            {
                count = 0;
            }
        }

        unverifiedShares.clear();
    }

    public long getCount()
    {
        return count;
    }

    public int getNumThreads()
    {
        return numThreads;
    }
}
