package mining_pool.verification.data;



import com.ampex.amperabase.IBlockAPI;
import com.ampex.amperabase.IKiAPI;

import java.math.BigInteger;

/**
 * Created by Queue on 1/22/2018.
 */
public class UnverifiedShare
{
    private long receivedTime = 0;
    private BigInteger receivedLocalHeight = null;
    private BigInteger receivedShareDifficulty = null;
    private String receivedPoolAddress = null;
    private IBlockAPI block = null;
    private IKiAPI ki;

    private boolean pplns = false;

    private boolean validUnverifiedShare = false;

    private UnverifiedShare(BigInteger _receivedLocalHeight, BigInteger _receivedShareDifficulty, String _receivedPoolAddress, IBlockAPI _block, boolean _pplns, IKiAPI _ki)
    {
        if (_receivedLocalHeight != null && _receivedShareDifficulty != null && _receivedPoolAddress != null && _block != null && _ki != null)
        {
            ki=_ki;
            receivedTime = System.currentTimeMillis();
            receivedLocalHeight = _receivedLocalHeight;
            receivedShareDifficulty = _receivedShareDifficulty;
            receivedPoolAddress = _receivedPoolAddress;
            block = _block;
            pplns = _pplns;
            validUnverifiedShare = true;
        }
    }

    public static UnverifiedShare create(BigInteger _receivedLocalHeight, BigInteger _receivedShareDifficulty, String _receivedPoolSolverID, IBlockAPI _block, boolean _pplns, IKiAPI _ki)
    {
        UnverifiedShare tempUnverifiedShare = new UnverifiedShare(_receivedLocalHeight, _receivedShareDifficulty, _receivedPoolSolverID, _block, _pplns, _ki);
        if (tempUnverifiedShare.isValidUnverifiedShare())
        {
            return tempUnverifiedShare;
        }

        return null;
    }

    public static UnverifiedShare create(BigInteger _receivedLocalHeight, BigInteger _receivedShareDifficulty, String _receivedPoolSolverID, IBlockAPI _block, IKiAPI _ki)
    {
        return create(_receivedLocalHeight, _receivedShareDifficulty, _receivedPoolSolverID, _block, false, _ki);
    }

    public long getReceivedTime()
    {
        return receivedTime;
    }

    public BigInteger getReceivedLocalHeight()
    {
        return receivedLocalHeight;
    }

    public BigInteger getReceivedShareDifficulty()
    {
        return receivedShareDifficulty;
    }

    public String getReceivedPoolAddress()
    {
        return receivedPoolAddress;
    }

    public IBlockAPI getBlock()
    {
        return block;
    }

    public boolean isPPLNS()
    {
        return pplns;
    }

    public boolean isValidUnverifiedShare()
    {
        return validUnverifiedShare;
    }

    public VerifiedShare verify()
    {
        return new VerifiedShare(this, ki);
    }
}
