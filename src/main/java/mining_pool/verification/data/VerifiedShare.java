package mining_pool.verification.data;

import amp.AmpConstants;
import com.ampex.amperabase.*;
import logging.IAmpexLogger;
import mining_pool.PoolLogging;
import mining_pool.database.data.IMinerID;
import mining_pool.database.data.MinerID;
import org.bouncycastle.jcajce.provider.digest.SHA3;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Queue on 1/20/2018.
 */
public class VerifiedShare
{
    private static final long MILLISECONDS_IN_SECOND = 1000;
    private static final long MILLISECONDS_IN_MINUTE = 60 * MILLISECONDS_IN_SECOND;

    public static final long GRACE = MILLISECONDS_IN_MINUTE * 2;

    private long timeStamp = 0;

    private IMinerID minerID = null;

    private String minerIDString = null;

    private byte[] hash = null;

    private BigInteger hashAsBigInt = null;

    private boolean pplns = false;

    private boolean validShare = false;

    private IKiAPI ki;

    VerifiedShare(UnverifiedShare _unverifiedShare, IKiAPI _ki)
    {
        ki=_ki;
        pplns = _unverifiedShare.isPPLNS();
        validShare = verifyTimeStamp(_unverifiedShare) && verifyHeight(_unverifiedShare) && verifyMinerID(_unverifiedShare) && verifyHash(_unverifiedShare) && verifyCoinBaseID(_unverifiedShare);
    }

    private boolean verifyTimeStamp(UnverifiedShare _unverifiedShare)
    {
        timeStamp = _unverifiedShare.getBlock().getTimestamp();

        long receivedTimeLessGrace = _unverifiedShare.getReceivedTime() - GRACE;

        long receivedTimePlusGrace = _unverifiedShare.getReceivedTime() + GRACE;

        boolean success = timeStamp > receivedTimeLessGrace && timeStamp < receivedTimePlusGrace;

        if (!success)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because of bad timestamp.");
                logger.warn("     Bad timestamp is: " + timeStamp);
                logger.warn("Received timestamp is: " + _unverifiedShare.getReceivedTime());
            }
        }

        return success;
    }

    private boolean verifyHeight(UnverifiedShare _unverifiedShare)
    {
        BigInteger shareHeight = _unverifiedShare.getBlock().getHeight();

        boolean success = _unverifiedShare.getReceivedLocalHeight().add(BigInteger.ONE).compareTo(shareHeight) == 0;

        if (!success)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because of bad height.");
                logger.warn("     Bad height is: " + shareHeight.toString(10));
                logger.warn("Received height is: " + _unverifiedShare.getReceivedLocalHeight().toString(10));
            }
        }

        return success;
    }

    private boolean verifyMinerID(UnverifiedShare _unverifiedShare)
    {
        boolean success = false;

        byte[] b64Bytes = Utils.fromBase64(_unverifiedShare.getBlock().getSolver());

        if(b64Bytes == null)
        {
            minerIDString = _unverifiedShare.getBlock().getSolver();
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because miner ID could not be converted from B64.");
                logger.warn("Value of block solver field was: " + _unverifiedShare.getBlock().getSolver());
            }
            return success;
        }

        IAddress address = ki.getAddMan().createFromByteArray(b64Bytes);

        minerID = MinerID.create(b64Bytes, ki);
        minerIDString = minerID.getIDAsString();

        success = address.isValid();

        if (!success)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because of bad miner ID.");
                logger.warn("Bad miner ID is: " + new String(b64Bytes, AmpConstants.UTF8));
            }
        }

        return success;
    }

    private boolean verifyHash(UnverifiedShare _unverifiedShare)
    {
        hash = hashBlockHeader(_unverifiedShare.getBlock().header().getBytes(AmpConstants.UTF8));

        hashAsBigInt = new BigInteger(hash);

        byte[] bigIntDiffByteArray = _unverifiedShare.getReceivedShareDifficulty().toByteArray();
        byte[] byteDiff = new byte[64];
        int p = 63;
        int k = bigIntDiffByteArray.length - 1;
        while (k >= 0)
        {
            if(p == -1) break;
            byteDiff[p] = bigIntDiffByteArray[k];
            k--;
            p--;
        }

        int mostSignificant0Digits = 0;
        for (int i = 0; i < byteDiff.length; i++)
        {
            mostSignificant0Digits = i;
            if (byteDiff[i] != 0)
            {
                break;
            }
        }
        int mostSignificantByte = byteDiff[mostSignificant0Digits] & 0x00ff;

        byte[] byteHash = hash;

        int precedingZeroes = 0;
        for (int i = 0; i < mostSignificant0Digits; i++)
        {
            precedingZeroes = (precedingZeroes | byteHash[i]) & 0x00ff;
        }

        boolean success = precedingZeroes == 0 && (byteHash[mostSignificant0Digits] & 0x00ff) <= mostSignificantByte;

        if (!success)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because of bad hash.");
            }
        }

        return success;
    }

    private boolean verifyCoinBaseID(UnverifiedShare _unverifiedShare)
    {
        boolean success = true;

        ITransAPI coinba = _unverifiedShare.getBlock().getCoinbase();

        List<IOutput> outputs = coinba.getOutputs();

        if (outputs.size() != 1)
        {
            success = false;
        } else
        {

            IOutput theOnlyOutput = outputs.get(0);

            String addressAsString = theOnlyOutput.getAddress().encodeForChain();

            String receivedPoolAddress = _unverifiedShare.getReceivedPoolAddress();

            success = addressAsString.equals(receivedPoolAddress);
        }


        if (!success)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.warn("Failed to verify share because of bad Coinbase Transaction.");
            }
        }

        return success;
    }

    private static byte[] hashBlockHeader(byte[] _header)
    {
        SHA3.DigestSHA3 md = new SHA3.Digest512();
        md.update(_header);
        return md.digest();
    }

    public long getTimeStamp()
    {
        return timeStamp;
    }

    public IMinerID getMinerID()
    {
        return minerID;
    }

    public String getMinerIDString()
    {
        return minerIDString;
    }

    public byte[] getHash()
    {
        return hash.clone();
    }

    public BigInteger getHashAsBigInt()
    {
        return hashAsBigInt;
    }

    public boolean isPPLNS()
    {
        return pplns;
    }

    public boolean isValidShare()
    {
        return validShare;
    }
}
