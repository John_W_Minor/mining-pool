package mining_pool.verification.data;

import amp.ByteTools;
import logging.IAmpexLogger;
import mining_pool.PoolLogging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Queue on 1/22/2018.
 */
public class UsedFullyVerifiedShares
{
    private Object obj = new Object();

    private HashMap<String, Object> usedSharesMap = new HashMap<>();
    private ArrayList<VerifiedShare> usedShares = new ArrayList<>();

    public boolean addVerifiedShare(VerifiedShare _share)
    {
        if (_share != null && _share.isValidShare())
        {
            String hash = ByteTools.buildHexString(_share.getHash());

            if (usedSharesMap.putIfAbsent(hash, obj) == null)
            {
                usedShares.add(_share);
                return true;
            }

            IAmpexLogger logger = PoolLogging.getLogger();
            if(logger!=null)
            {
                logger.warn("Share from miner: " + _share.getMinerIDString() + " was a repeat of another share.");
            }
        }

        if(_share != null && !_share.isValidShare())
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if(logger!=null)
            {
                logger.warn("Share from miner: " + _share.getMinerIDString() + " was not valid.");
            }
        }

        if(_share == null)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if(logger!=null)
            {
                logger.warn("Share was null. What the hell happened?");
            }
        }

        return false;
    }

    public void prune()
    {
        long pruneStamp = System.currentTimeMillis() - (VerifiedShare.GRACE + VerifiedShare.GRACE/2);

        Iterator<VerifiedShare> sharesIter = usedShares.iterator();
        while (sharesIter.hasNext())
        {
            VerifiedShare share = sharesIter.next();
            if (pruneStamp > share.getTimeStamp())
            {
                String hash = ByteTools.buildHexString(share.getHash());
                usedSharesMap.remove(hash);
                sharesIter.remove();
            }
        }
    }
}
