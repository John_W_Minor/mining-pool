package mining_pool;

import amp.AmpConstants;
import com.ampex.amperabase.*;
import com.ampex.amperabase.data.WLong;
import logging.IAmpexLogger;
import mining_pool.database.Database;
import mining_pool.database.PPLNSMemoryDatabase;
import mining_pool.database.data.*;
import mining_pool.events.IPoolEventHandler;
import mining_pool.events.PoolEventType;
import mining_pool.verification.Verifier;
import mining_pool.verification.data.UnverifiedShare;
import mining_pool.verification.data.VerifiedShare;

import java.math.BigInteger;
import java.util.*;

/**
 * Created by Queue on 1/23/2018.
 */
public class Pool extends Thread implements IPool
{
    private final Object shareLock = new Object();

    private final Object dbLock = new Object();

    private final Object pplnsShareLock = new Object();

    private static final long MILLISECONDS_IN_SECOND = 1000;
    private static final long MILLISECONDS_IN_MINUTE = 60 * MILLISECONDS_IN_SECOND;
    private static final long MILLISECONDS_IN_HOUR = 60 * MILLISECONDS_IN_MINUTE;

    private volatile Verifier vFier = new Verifier();

    private volatile Database dBase;
    private volatile PPLNSMemoryDatabase pplnsDBase = new PPLNSMemoryDatabase();

    private volatile BigInteger currentLocalHeight = null;
    private volatile ITimeRange currentTimeRange = null;
    private volatile BigInteger currentShareDifficulty = null;
    private volatile IKiAPI ki = null;

    private volatile WLong currentPayPerShare = new WLong();

    private volatile long currentPayInterval = MILLISECONDS_IN_HOUR;

    private volatile boolean receiptsAvailable = false;


    private volatile ArrayList<IAddress> shareVerificationReport = new ArrayList<>();

    private volatile Set<IPoolEventHandler> eventHandlers = new HashSet<>();

    private volatile List<VerifiedShare> lastNSharesUnreliable;
    private volatile ITimeRange lastNSharesUnreliableTimeRange;
    private volatile long lastNSharesUnreliableTimeout = 6000;

    private volatile boolean lockDebug = true;



    //==========================ADDED BY BRYAN
    @Override
    public IKiAPI getGod()
    {
        return ki;
    }

    public static IKiAPI getStaticGod()
    {
        return staticKi;
    }
    private static IKiAPI staticKi;
    //==========================END OF ADDED BY BRYAN

    public Pool(BigInteger _currentHeight, BigInteger _currentShareDifficulty, long _currentPayPerShare, IKiAPI _ki)
    {
        setName("Pool Thread");

        currentLocalHeight = _currentHeight;
        currentShareDifficulty = _currentShareDifficulty;
        currentPayPerShare.set(_currentPayPerShare);

        ki = _ki;
        staticKi = _ki;

        dBase = new Database(ki);

        checkCurrentPayPeriod();
    }

    public Pool(BigInteger _currentHeight, BigInteger _currentShareDifficulty, long _currentPayPerShare, IKiAPI _ki, IPoolEventHandler _eventHandler)
    {
        this(_currentHeight, _currentShareDifficulty, _currentPayPerShare, _ki);
        eventHandlers.add(_eventHandler);
    }

    public Pool(BigInteger _currentHeight, BigInteger _currentShareDifficulty, long _currentPayPerShare, IKiAPI _ki, List<IPoolEventHandler> _eventHandlers)
    {
        this(_currentHeight, _currentShareDifficulty, _currentPayPerShare, _ki);

        eventHandlers.addAll(_eventHandlers);
    }

    @Override
    public void registerEventHandler(IPoolEventHandler _eventHandler)
    {
        eventHandlers.add(_eventHandler);
    }

    @Override
    public void registerEventHandlers(List<IPoolEventHandler> _eventHandlers)
    {
        eventHandlers.addAll(_eventHandlers);
    }

    @Override
    public void updateCurrentHeight(BigInteger _currentLocalHeight)
    {
        currentLocalHeight = _currentLocalHeight;

        for (IPoolEventHandler handler : eventHandlers)
        {
            try
            {
                handler.handle(this, PoolEventType.NEW_HEIGHT);
            } catch (Exception e)
            {
                IAmpexLogger logger = PoolLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
        }
    }

    @Override
    public void updateCurrentShareDifficulty(BigInteger _currentShareDifficulty)
    {
        currentShareDifficulty = _currentShareDifficulty;

        for (IPoolEventHandler handler : eventHandlers)
        {
            try
            {
                handler.handle(this, PoolEventType.NEW_SHARE_DIFFICULTY);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateCurrentPayPerShare(long _currentPayPerShare)
    {
        currentPayPerShare.set(_currentPayPerShare);

        for (IPoolEventHandler handler : eventHandlers)
        {
            try
            {
                handler.handle(this, PoolEventType.NEW_PAY_PER_SHARE);
            } catch (Exception e)
            {
                IAmpexLogger logger = PoolLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
        }
    }

    @Override
    public ITimeRange getCurrentTimeRange()
    {
        return currentTimeRange;
    }

    @Override
    public void updateCurrentPayInterval(long _currentPayInterval)
    {
        currentPayInterval = _currentPayInterval;

        for (IPoolEventHandler handler : eventHandlers)
        {
            try
            {
                handler.handle(this, PoolEventType.NEW_PAY_INTERVAL);
            } catch (Exception e)
            {
                IAmpexLogger logger = PoolLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
        }
    }

    @Override
    public void addShare(IBlockAPI _block)
    {
        UnverifiedShare tempUnverifiedShare = UnverifiedShare.create(currentLocalHeight, currentShareDifficulty, ki.getAddMan().getMainAdd().encodeForChain(), _block, ki);

        if (tempUnverifiedShare != null)
        {
            vFier.addUnverifiedShare(tempUnverifiedShare);

            synchronized (shareLock)
            {
                shareLock.notifyAll();
            }
        }
    }

    @Override
    public void addPPLNSShare(IBlockAPI _block)
    {
        UnverifiedShare tempUnverifiedShare = UnverifiedShare.create(currentLocalHeight, currentShareDifficulty, ki.getAddMan().getMainAdd().encodeForChain(), _block, true, ki);

        if (tempUnverifiedShare != null)
        {
            vFier.addUnverifiedShare(tempUnverifiedShare);

            synchronized (shareLock)
            {
                shareLock.notifyAll();
            }
        }
    }

    private void processShares()
    {
//        PoolLogging.getLogger().info("Processing shares");
        ArrayList<VerifiedShare> verifiedShares = vFier.verify();

        //System.out.println(verifiedShares.size() + " shares verified.\n");

        attemptDBLockAcquire("processShares()");
        synchronized (dbLock)
        {
            successDBLockAcquire("processShares()");

            try
            {
                for (VerifiedShare verifiedShare : verifiedShares)
                {
                    if (!verifiedShare.isPPLNS())
                    {
                        IMinerID tempMinerID = verifiedShare.getMinerID();
                        shareVerificationReport.add(tempMinerID.getIDAsAddress());
                        dBase.incrementTotalSharesOfMiner(tempMinerID, currentTimeRange);
                        dBase.incrementTotalSharesOfPayPeriod(currentTimeRange);
                    } else
                    {
                        pplnsDBase.addPPLNSShare(verifiedShare);
                    }
                }
            } catch (Exception e)
            {
                IAmpexLogger logger = PoolLogging.getLogger();
                if (logger != null)
                {
                    logger.error("Mining pool database corrupted.", e);
                    logger.info("Building new database.");
                }
                dBase.nukeDatabase();
            }

            attemptDBLockRelease("processShares()");
        }
        successDBLockRelease("processShares()");
    }

    private void checkCurrentPayPeriod()
    {
        attemptDBLockAcquire("checkCurrentPayPeriod()");
        synchronized (dbLock)
        {
            successDBLockAcquire("checkCurrentPayPeriod()");

            IAmpexLogger logger = PoolLogging.getLogger();

            try
            {
                ArrayList<ITimeRange> timeRanges = null;

                try
                {
                    timeRanges = dBase.getTimeRanges();
                } catch (Exception e)
                {
                    if (logger != null)
                    {
                        logger.error("Mining pool database corrupted.", e);
                    }
                    timeRanges = null;
                }

                if (timeRanges != null && timeRanges.size() > 0)
                {
                    if (logger != null)
                    {
                        logger.info("Found time ranges.");
                    }

                    int count = timeRanges.size();
                    currentTimeRange = timeRanges.get(count - 1);

                    if (currentTimeRange.isExpired(currentPayInterval))
                    {
                        if (logger != null)
                        {
                            logger.info("Current time range expired.");
                        }
                        ITimeRange[] tallyRange = new ITimeRange[count];

                        for (int i = 0; i < count; i++)
                        {
                            tallyRange[i] = timeRanges.get(i);
                        }

                        try
                        {
                            tallyDebts(tallyRange);
                            if (logger != null)
                            {
                                logger.info("Debts tallied.");
                            }
                        } catch (Exception e)
                        {
                            if (logger != null)
                            {
                                logger.error("Mining pool database corrupted.", e);
                                logger.info("Building new database.");
                            }
                            dBase.nukeDatabase();
                        }

                        timeRanges.clear();

                        if (logger != null)
                        {
                            logger.info("Starting new pay period.");
                        }

                        currentTimeRange = new TimeRange();

                        timeRanges.add(currentTimeRange);

                        dBase.putTimeRanges(timeRanges);

                        dBase.resetConnections();
                    }
                } else
                {
                    if (logger != null)
                    {
                        logger.info("Building new database.");
                        logger.info("Starting new pay period.");
                    }
                    dBase.nukeDatabase();

                    currentTimeRange = new TimeRange();

                    timeRanges = new ArrayList<>();
                    timeRanges.add(currentTimeRange);

                    dBase.putTimeRanges(timeRanges);

                    dBase.resetConnections();
                }
            } catch (Exception e)
            {
                if (logger != null)
                {
                    logger.error("", e);
                }
            }

            attemptDBLockRelease("checkCurrentPayPeriod()");
        }

        successDBLockRelease("checkCurrentPayPeriod()");

        if (receiptsAvailable)
        {
            for (IPoolEventHandler handler : eventHandlers)
            {
                try
                {
                    handler.handle(this, PoolEventType.RECEIPTS_AVAILABLE);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }

        if (shareVerificationReport.size() != 0)
        {
            for (IPoolEventHandler handler : eventHandlers)
            {
                try
                {
                    handler.handle(this, PoolEventType.SHARE_VERIFICATION_REPORT_AVAILABLE);
                } catch (Exception e)
                {
                    IAmpexLogger logger = PoolLogging.getLogger();
                    if (logger != null)
                    {
                        logger.error("", e);
                    }
                }
            }

            shareVerificationReport.clear();
        }
    }

    private void tallyDebts(ITimeRange[] _timeRanges) throws Exception
    {
        for (ITimeRange timeRange : _timeRanges)
        {
            ArrayList<MinerIndex> minerIndices = dBase.getMinerIndices(timeRange);

            if (minerIndices != null)
            {
                for (MinerIndex minerIndex : minerIndices)
                {
                    int count = minerIndex.getNumberOfEntries();

                    for (int i = 0; i < count; i++)
                    {
                        IMinerID payee = minerIndex.getElement(i);

                        Long shares = dBase.getTotalSharesOfMiner(payee, timeRange);

                        if (payee == null || shares == null)
                        {
                            throw new Exception();
                        }

                        long originToPay = shares * currentPayPerShare.get();

                        IPaymentReceipt receipt = PaymentReceipt.create(payee, originToPay);

                        if(!dBase.pushPaymentReceipt(receipt))
                        {
                            if(PoolLogging.getLogger() != null)
                            PoolLogging.getLogger().warn("Failed to push payment receipt to database for payee: " + receipt.getPayee().getIDAsString());
                        }
                    }
                }
            }

            dBase.deleteMetaDatabase(timeRange.getRangeAsString());
            dBase.deleteSharesDatabase(timeRange.getRangeAsString());
        }

        receiptsAvailable = true;
    }

    @Override
    public void endPPLNSRound(List<IBlockAPI> _blocksFoundInRound, double _poolFee, IKiAPI _ki)
    {
        if (_blocksFoundInRound == null || _blocksFoundInRound.size() == 0)
        {
            return;
        }

        ArrayList<VerifiedShare> pplnsShares = pplnsDBase.getLastNShares();

        if (pplnsShares.size() == 0)
        {
            return;
        }

        attemptDBLockAcquire("endPPLNSRound()");
        synchronized (dbLock)
        {
            successDBLockAcquire("endPPLNSRound()");

            try
            {
                ArrayList<PPLNSSharesPerMiner> sharesPerMinerObjects = new ArrayList<>();

                HashMap<String, IMinerID> minerIDsMap = new HashMap<>();

                for (VerifiedShare verifiedShare : pplnsShares)
                {
                    IMinerID tempMinerID = verifiedShare.getMinerID();
                    minerIDsMap.putIfAbsent(tempMinerID.getIDAsString(), tempMinerID);
                }

                for (String key : minerIDsMap.keySet())
                {
                    IMinerID minerID = minerIDsMap.get(key);
                    String payeeStringID = minerID.getIDAsString();
                    long shareCount = 0;

                    Iterator<VerifiedShare> pplnsSharesIter = pplnsShares.iterator();
                    while (pplnsSharesIter.hasNext())
                    {
                        VerifiedShare tempShare = pplnsSharesIter.next();
                        String tempMinerStringID = tempShare.getMinerID().getIDAsString();
                        if (payeeStringID.equals(tempMinerStringID))
                        {
                            shareCount++;
                            pplnsSharesIter.remove();
                        }
                    }

                    PPLNSSharesPerMiner tempSharesPerMiner = PPLNSSharesPerMiner.create(minerID, shareCount);
                    sharesPerMinerObjects.add(tempSharesPerMiner);
                }

                ArrayList<BlockID> blockIDs = new ArrayList<>();

                for (IBlockAPI block : _blocksFoundInRound)
                {
                    BlockID tempBlockID = BlockID.create(block.getID().getBytes(AmpConstants.UTF8), block.getHeight());
                    blockIDs.add(tempBlockID);
                }

                PPLNSRoundShareLog log = PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, System.currentTimeMillis(), _poolFee);

                try
                {
                    ArrayList<PPLNSRoundShareLog> dBaseLogs = dBase.getRoundShareLogs();
                    dBaseLogs.add(log);
                    dBase.putRoundShareLogs(dBaseLogs);
                } catch (Exception e)
                {
                    IAmpexLogger logger = PoolLogging.getLogger();
                    if (logger != null)
                    {
                        logger.error("Mining pool database corrupted.", e);
                        logger.info("Building new database.");
                    }
                    dBase.nukeDatabase();
                }

                tallyPPLNSDebts(_ki);
            } catch (Exception e)
            {
                IAmpexLogger logger = PoolLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
            attemptDBLockRelease("endPPLNSRound()");
        }
        successDBLockRelease("endPPLNSRound()");

        if (receiptsAvailable)
        {
            for (IPoolEventHandler handler : eventHandlers)
            {
                try
                {
                    handler.handle(this, PoolEventType.RECEIPTS_AVAILABLE);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    private void tallyPPLNSDebts(IKiAPI _ki)
    {
//        System.out.println("Tallying PPLNS debts");
        try
        {
            ArrayList<PPLNSRoundShareLog> dBaseLogs = dBase.getRoundShareLogs();

            if (dBaseLogs.size() == 0)
            {
                return;
            }

            Iterator<PPLNSRoundShareLog> dBaseLogsIter = dBaseLogs.iterator();

            while (dBaseLogsIter.hasNext())
            {
                PPLNSRoundShareLog log = dBaseLogsIter.next();

                long now = System.currentTimeMillis();
                long diff = now - log.getRoundEndTime();
//                if(diff <= MILLISECONDS_IN_HOUR)
//                {
//                    System.out.println("what the fuck is this control flow doing");
//                }
//                if (diff > MILLISECONDS_IN_HOUR)
//                {
                    BigInteger rewardBI = BigInteger.ZERO;

                    ArrayList<BlockID> blocksFoundInRound = log.getBlockIDs();

                    for (BlockID blockID : blocksFoundInRound)
                    {
                        String id = blockID.getIDAsString();

                        IBlockAPI tempBlock = _ki.getChainMan().getByHeight(blockID.getHeight());

                        if (tempBlock == null || !tempBlock.getID().equals(id))
                        {
                            continue;
                        }

                        boolean success = false;

                        ITransAPI coinba = tempBlock.getCoinbase();

                        List<IOutput> outputs = coinba.getOutputs();

                        if (outputs.size() != 1)
                        {
                            success = false;
                        } else
                        {

                            IOutput theOnlyOutput = outputs.get(0);

                            String addressAsString = theOnlyOutput.getAddress().encodeForChain();

                            List<IAddress> poolAddresses = ki.getAddMan().getAll();

                            for (IAddress address : poolAddresses)
                            {
                                if (addressAsString.equals(address.encodeForChain()))
                                {
                                    success = true;
                                    break;
                                }
                            }
                        }

                        if (success)
                        {
                            rewardBI = rewardBI.add(BlockDataUtils.blockRewardForHeight(tempBlock.getHeight()));

                            for (String key : tempBlock.getTransactionKeys())
                            {
                                ITransAPI trans = tempBlock.getTransaction(key);

                                rewardBI = rewardBI.add(trans.getFee());
                            }
                        }
                    }

                    long sharesCount = 0;
                    for (PPLNSSharesPerMiner sharesPerMinerObject : log.getSharesPerMinerObjects())
                    {
                        sharesCount += sharesPerMinerObject.getShares();
                    }

                    BigInteger ppsBI = rewardBI.divide(BigInteger.valueOf(sharesCount));
//                    System.out.println("rw: " + rewardBI + " sc: " + sharesCount + " ppsbi: " + ppsBI + " lv: " + ppsBI.longValue() + " fee: " + log.getPoolFee());
                    long pps = ppsBI.longValue();
                    pps -= (long) (pps * log.getPoolFee());
                    if (pps > 0)
                    {
                        for (PPLNSSharesPerMiner sharesPerMinerObject : log.getSharesPerMinerObjects())
                        {
                            IMinerID payee = sharesPerMinerObject.getMinerID();
                            long originToPay = sharesPerMinerObject.getShares() * pps;

                            IPaymentReceipt tempReceipt = PaymentReceipt.create(payee, originToPay);

                            if(!dBase.pushPaymentReceipt(tempReceipt))
                            {
                                if(PoolLogging.getLogger() != null)
                                PoolLogging.getLogger().warn("Failed to push payment receipt to database for payee: " + tempReceipt.getPayee().getIDAsString());
                            }
                        }
                    }

                    dBaseLogsIter.remove();
//                }
            }
//            System.out.println("Debts tallied");
            dBase.putRoundShareLogs(dBaseLogs);

            receiptsAvailable = true;

        } catch (Exception e)
        {
            e.printStackTrace();
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.error("Mining pool database corrupted.", e);
                logger.info("Building new database.");
            }
            dBase.nukeDatabase();
        }
    }

    @Override
    public long getN()
    {
        return pplnsDBase.getN();
    }

    @Override
    public void setN(long _n)
    {
        pplnsDBase.setN(_n);
    }

    @Override
    public WLong getCurrentPayPerShare()
    {
        return currentPayPerShare;
    }

    @Override
    public ArrayList<IAddress> getShareVerificationReport()
    {
        ArrayList<IAddress> copyShareVerificationReport = new ArrayList<>();

        attemptDBLockAcquire("getShareVerificationReport()");
        synchronized (dbLock)
        {
            successDBLockAcquire("getShareVerificationReport()");

            copyShareVerificationReport.addAll(shareVerificationReport);

            attemptDBLockRelease("getShareVerificationReport()");
        }
        successDBLockRelease("getShareVerificationReport()");

        return copyShareVerificationReport;
    }

    @Override
    public IPaymentReceipt popReceipt()
    {
        if (receiptsAvailable)
        {
            IPaymentReceipt result = null;

            attemptDBLockAcquire("popReceipt()");
            synchronized (dbLock)
            {
                successDBLockAcquire("popReceipt()");

                try
                {
                    result = dBase.popPaymentReceipt();
                    dBase.peekPaymentReceipt();
                } catch (Exception e)
                {
                    IAmpexLogger logger = PoolLogging.getLogger();
                    if (logger != null)
                    {
                        logger.error("Mining pool database corrupted.", e);
                        logger.info("Building new database.");
                    }
                    dBase.nukeDatabase();
                    result = null;
                }

                attemptDBLockRelease("popReceipt()");
            }
            successDBLockRelease("popReceipt()");

            if (result == null)
            {
                receiptsAvailable = false;
            }

            return result;
        }
        return null;
    }

    @Override
    public IPaymentReceipt peekReceipt()
    {
        if (receiptsAvailable)
        {
            IPaymentReceipt result = null;

            attemptDBLockAcquire("peekReceipt()");
            synchronized (dbLock)
            {
                successDBLockAcquire("peekReceipt()");

                try
                {
                    result = dBase.peekPaymentReceipt();
                } catch (Exception e)
                {
                    IAmpexLogger logger = PoolLogging.getLogger();
                    if (logger != null)
                    {
                        logger.error("Mining pool database corrupted.", e);
                        logger.info("Building new database.");
                    }
                    dBase.nukeDatabase();
                    result = null;
                }

                attemptDBLockRelease("peekReceipt()");
            }
            successDBLockRelease("peekReceipt()");

            if (result == null)
            {
                receiptsAvailable = false;
            }

            return result;
        }
        return null;
    }

    @Override
    public long getNumberOfLiveReceipts()
    {
        long result;

        attemptDBLockAcquire("getNumberOfLiveReceipts()");
        synchronized (dbLock)
        {
            successDBLockAcquire("getNumberOfLiveReceipts()");

            result = dBase.getCurrentReceiptIndex() + 1;

            attemptDBLockRelease("getNumberOfLiveReceipts()");
        }
        successDBLockRelease("getNumberOfLiveReceipts()");

        return result;
    }

    @Override
    public IPaymentReceipt getReceiptByIndex(long _index)
    {
        IPaymentReceipt result = null;

        attemptDBLockAcquire("getReceiptByIndex()");
        synchronized (dbLock)
        {
            successDBLockAcquire("getReceiptByIndex()");

            try
            {
                result = dBase.getPaymentReceiptByIndex(_index);
            } catch (Exception e)
            {
                e.printStackTrace();
                result = null;
            }

            attemptDBLockRelease("getReceiptByIndex()");
        }
        successDBLockRelease("getReceiptByIndex()");

        return result;
    }

    @Override
    public boolean areReceiptsAvailable()
    {
        return receiptsAvailable;
    }

    @Override
    public long getTotalSharesOfMiner(IAddress _address)
    {
        IMinerID tempMinerID = MinerID.create(_address.toByteArray(), ki);

        if (tempMinerID == null)
        {
            //System.out.println("ID from address did not convert to a proper MinerID object.");
            //System.out.println("ID from address was " + _address.encodeForChain().getBytes().length + " bytes long.");
            return 0;
        }

        Long result;

        ///*
        attemptDBLockAcquire("getTotalSharesOfMiner()");
        synchronized (dbLock)
        {
            successDBLockAcquire("getTotalSharesOfMiner()");
            //*/
            result = dBase.getTotalSharesOfMiner(tempMinerID, currentTimeRange);
            ///*
            attemptDBLockRelease("getTotalSharesOfMiner()");
        }
        successDBLockRelease("getTotalSharesOfMiner()");
        //*/

        if (result == null)
        {
            return 0;
        }

        return result;
    }

    @Override
    public long getTotalPPLNSSharesOfMiner(IAddress _address)
    {
        if (_address == null)
        {
            return 0;
        }

        IMinerID tempMinerID = MinerID.create(_address.toByteArray(), ki);

        if (tempMinerID == null)
        {
            return 0;
        }

        long amount = 0;

        synchronized (pplnsShareLock)
        {
            if (lastNSharesUnreliableTimeRange == null || lastNSharesUnreliableTimeRange.isExpired(lastNSharesUnreliableTimeout))
            {
                lastNSharesUnreliableTimeRange = new TimeRange();
                lastNSharesUnreliable = pplnsDBase.getLastNShares();
            }

            for (VerifiedShare vShare : lastNSharesUnreliable)
            {
                if (tempMinerID.getIDAsString().equals(vShare.getMinerIDString()))
                {
                    amount++;
                }
            }
        }

        return amount;
    }

    @Override
    public WLong getTotalSharesOfCurrentPayPeriod()
    {
        WLong result = new WLong();

        ///*
        attemptDBLockAcquire("getTotalSharesOfCurrentPayPeriod()");
        synchronized (dbLock)
        {
            successDBLockAcquire("getTotalSharesOfCurrentPayPeriod()");
            //*/
            result.set(dBase.getTotalSharesOfPayPeriod(currentTimeRange));

            ///*
            attemptDBLockRelease("getTotalSharesOfCurrentPayPeriod()");
        }
        successDBLockRelease("getTotalSharesOfCurrentPayPeriod()");
        //*/

        return result;
    }

    //region lock debug
    private static final String ATTEMPTING_TO_LOCK_ON_DBLOCK = " is attempting to acquire the dbLock.";
    private static final String SUCCESSFUL_LOCK_ON_DBLOCK = " has acquired the dbLock.";
    private static final String ATTEMPTING_TO_UNLOCK_ON_DBLOCK = " is attempting to release the dbLock.";
    private static final String SUCCESSFUL_UNLOCK_ON_DBLOCK = " has released the dbLock.";

    private static final String ATTEMPTING_TO_LOCK_ON_SHARELOCK = " is attempting to acquire the shareLock.";
    private static final String SUCCESSFUL_LOCK_ON_SHARELOCK = " has acquired the shareLock.";
    private static final String ATTEMPTING_TO_UNLOCK_ON_SHARELOCK = " is attempting to release the shareLock.";
    private static final String SUCCESSFUL_UNLOCK_ON_SHARELOCK = " has released the shareLock.";

    private void attemptDBLockAcquire(String _methodName)
    {
        if (lockDebug)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                //logger.debug(_methodName + ATTEMPTING_TO_LOCK_ON_DBLOCK);
            }
        }
    }

    private void successDBLockAcquire(String _methodName)
    {
        if (lockDebug)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                //logger.debug(_methodName + SUCCESSFUL_LOCK_ON_DBLOCK);
            }
        }
    }

    private void attemptDBLockRelease(String _methodName)
    {
        if (lockDebug)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                //logger.debug(_methodName + ATTEMPTING_TO_UNLOCK_ON_DBLOCK);
            }
        }
    }

    private void successDBLockRelease(String _methodName)
    {
        if (lockDebug)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                //logger.debug(_methodName + SUCCESSFUL_UNLOCK_ON_DBLOCK);
            }
        }
    }

    @Override
    public void enableLockDebug()
    {
        lockDebug = true;
    }

    @Override
    public void disableLockDebug()
    {
        lockDebug = false;
    }

    /*
    private void attemptShareLockAcquire(String _methodName)
    {
        Logger logger = PoolLogging.getLogger();
        if (logger != null)
        {
            logger.debug(_methodName + ATTEMPTING_TO_LOCK_ON_SHARELOCK);
        }
    }

    private void successShareLockAcquire(String _methodName)
    {
        Logger logger = PoolLogging.getLogger();
        if (logger != null)
        {
            logger.debug(_methodName + SUCCESSFUL_LOCK_ON_SHARELOCK);
        }
    }

    private void attemptShareLockRelease(String _methodName)
    {
        Logger logger = PoolLogging.getLogger();
        if (logger != null)
        {
            logger.debug(_methodName + ATTEMPTING_TO_UNLOCK_ON_SHARELOCK);
        }
    }

    private void successShareLockRelease(String _methodName)
    {
        Logger logger = PoolLogging.getLogger();
        if (logger != null)
        {
            logger.debug(_methodName + SUCCESSFUL_UNLOCK_ON_SHARELOCK);
        }
    }
    //*/
    //endregion

    @Override
    public void run()
    {
        Thread shareProcessor = new Thread()
        {
            public void run()
            {
                while (true)
                {
                    if (Thread.interrupted())
                    {
                        break;
                    }

                    synchronized (shareLock)
                    {
                        try
                        {
                            shareLock.wait();
                        } catch (Exception e)
                        {
                            break;
                        }
                    }

                    if (Thread.interrupted())
                    {
                        break;
                    }

                    processShares();
                }
            }
        };

        shareProcessor.setName("Pool Share Processor Thread");

        shareProcessor.start();

        while (true)
        {
            if (Thread.interrupted())
            {
                break;
            }

            try
            {
                Thread.sleep(60000);
            } catch (Exception e)
            {
                shareProcessor.interrupt();
                break;
            }

            if (Thread.interrupted())
            {
                shareProcessor.interrupt();
                break;
            }

            checkCurrentPayPeriod();
        }
    }
}
