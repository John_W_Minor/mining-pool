package mining_pool.database.data;

import amp.HeadlessPrefixedAmplet;
import amp.serialization.IAmpByteSerializable;
import com.ampex.amperabase.IKiAPI;

import java.util.ArrayList;

/**
 * Created by Queue on 1/15/2018.
 */
public class MinerIndex implements IAmpByteSerializable
{
    public static final int MAX_ENTRIES = 1000;

    private ArrayList<IMinerID> minerIDs = new ArrayList<>();

    private int id = 0;

    private ITimeRange timeRange = null;

    private boolean validMinerIndex = false;

    private MinerIndex(int _id, ITimeRange _timeRange)
    {
        if (_timeRange != null)
        {
            id = _id;
            timeRange = _timeRange;
            validMinerIndex = true;
        }
    }

    public static MinerIndex create(int _id, ITimeRange _timeRange)
    {
        MinerIndex tempMinerIndex = new MinerIndex(_id, _timeRange);
        if (tempMinerIndex.isValidMinerIndex())
        {
            return tempMinerIndex;
        }
        return null;
    }

    public boolean addElement(IMinerID _minerID)
    {
        if(_minerID != null)
        {
            int entriesCount = minerIDs.size();

            if (entriesCount < MAX_ENTRIES)
            {
                minerIDs.add(_minerID);
                return true;
            }
        }
        return false;
    }

    public IMinerID getElement(int _index)
    {
        if (_index < minerIDs.size() && _index >= 0)
        {
            return minerIDs.get(_index);
        }

        return null;
    }

    public int getNumberOfEntries()
    {
        return minerIDs.size();
    }

    public int getId()
    {
        return id;
    }

    public ITimeRange getTimeRange()
    {
        return timeRange;
    }

    public boolean isValidMinerIndex()
    {
        return validMinerIndex;
    }

    public boolean isFull()
    {
        int entriesCount = minerIDs.size();

        return !(entriesCount < MAX_ENTRIES);
    }

    public byte[] serializeToBytes()
    {
        if (validMinerIndex)
        {
            HeadlessPrefixedAmplet minerIndexHPA = HeadlessPrefixedAmplet.create();

            for (IMinerID minerID : minerIDs)
            {
                minerIndexHPA.addElement(minerID);
            }

            return minerIndexHPA.serializeToBytes();
        }

        return null;
    }

    public static MinerIndex deserializeFromBytes(byte[] _minerIndexBytes, int _id, ITimeRange _timeRange, IKiAPI _ki)
    {
        if (_minerIndexBytes != null && _timeRange != null)
        {
            HeadlessPrefixedAmplet minerIndexHPA = HeadlessPrefixedAmplet.create(_minerIndexBytes);

            if (minerIndexHPA != null)
            {
                int idCount = 0;

                MinerIndex minerIndex = MinerIndex.create(_id, _timeRange);

                while(true)
                {
                    byte[] minerIDBytes = minerIndexHPA.getNextElement();

                    if(minerIDBytes == null)
                    {
                        break;
                    }

                    IMinerID tempMinerID = MinerID.create(minerIDBytes, _ki);

                    minerIndex.addElement(tempMinerID);

                    idCount++;
                }

                if(idCount <= MAX_ENTRIES)
                {
                    return minerIndex;
                }
            }
        }

        return null;
    }
}
