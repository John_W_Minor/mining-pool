package mining_pool.database.data;

import amp.AmpConstants;
import amp.Amplet;
import amp.HeadlessAmplet;
import amp.classification.AmpClassCollection;
import amp.classification.classes.AC_SingleElement;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpByteSerializable;

import java.math.BigInteger;

/**
 * Created by Queue on 2/18/2018.
 */
public class BlockID implements IAmpAmpletSerializable
{
    private byte[] id = null;
    private BigInteger height = null;

    private boolean valid = false;

    private BlockID(byte[] _id, BigInteger _height)
    {
        if (_id != null && _height != null)
        {
            id = _id.clone();
            height = _height;
            valid = true;
        }
    }

    public static BlockID create(byte[] _id, BigInteger _height)
    {
        BlockID tempBlockID = new BlockID(_id, _height);
        if (tempBlockID.isValid())
        {
            return tempBlockID;
        }
        return null;
    }

    public byte[] getID()
    {
        return id.clone();
    }

    public BigInteger getHeight()
    {
        return height;
    }

    public String getIDAsString()
    {
        return new String(id, AmpConstants.UTF8);
    }

    public boolean isValid()
    {
        return valid;
    }

    public static final GroupID ID_GI = new GroupID(4522666, 666, "IBlockAPI ID.");
    public static final GroupID HEIGHT_GI = new GroupID(1234, 222, "IBlockAPI Height.");

    public Amplet serializeToAmplet()
    {
        AC_SingleElement idAC = AC_SingleElement.create(ID_GI, id);
        AC_SingleElement heightAC = AC_SingleElement.create(HEIGHT_GI, height);

        AmpClassCollection blockIDCC = new AmpClassCollection();

        blockIDCC.addClass(idAC);
        blockIDCC.addClass(heightAC);

        return blockIDCC.serializeToAmplet();
    }

    public static BlockID deserializeFromAmplet(Amplet _blockIDAmplet)
    {
        if (_blockIDAmplet == null)
        {
            return null;
        }

        UnpackedGroup blockIDUG = _blockIDAmplet.unpackGroup(ID_GI);

        if (blockIDUG == null)
        {
            return null;
        }

        byte[] id = blockIDUG.getElement(0);

        UnpackedGroup heightUG = _blockIDAmplet.unpackGroup(HEIGHT_GI);

        if (heightUG == null)
        {
            return null;
        }

        BigInteger height = heightUG.getElementAsBigInteger(0);

        return BlockID.create(id, height);
    }
}
