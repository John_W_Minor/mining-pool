package mining_pool.database.data;

import amp.AmpConstants;
import amp.ByteTools;
import com.ampex.amperabase.IAddress;
import com.ampex.amperabase.IKiAPI;
import logging.IAmpexLogger;
import mining_pool.Pool;
import mining_pool.PoolLogging;

/**
 * Created by Queue on 1/15/2018.
 */
public class MinerID implements IMinerID
{
    //public static final int ID_SIZE = 47 * AmpConstants.BYTE_BYTE_FOOTPRINT;

    private byte[] id = null;

    private boolean validMinerID = false;
    private IKiAPI ki;

    private MinerID(byte[] _id, IKiAPI _ki)
    {
        if (_id != null && _ki != null)
        {
            ki=_ki;
            id = _id.clone();
            validMinerID = true;
        }
    }

    public static IMinerID create(byte[] _id, IKiAPI _ki)
    {
        MinerID tempMinerID = new MinerID(_id, _ki);
        if (tempMinerID.validMinerID)
        {
            return tempMinerID;
        }

        return null;
    }

    @Override
    public String getIDAsHexString()
    {
        return ByteTools.buildHexString(id);
    }

    @Override
    public String getIDAsString()
    {
        IAddress address = Pool.getStaticGod().getAddMan().createFromByteArray(id);// TODO CONVERT THIS TO USE IKiAPI.getAddMan().createFromByteArray()

        if(address == null)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if(logger != null)
            {
                logger.warn("Error converting ID to String through Addres.fromByteArray.\nThrough new String the ID is:" + new String(id, AmpConstants.UTF8));
            }
            return null;
        }

        return address.encodeForChain();
    }

    @Override
    public IAddress getIDAsAddress()
    {
        return ki.getAddMan().decodeFromChain(getIDAsString());
    }

    @Override
    public boolean isValidMinerID()
    {
        return validMinerID;
    }

    @Override
    public byte[] serializeToBytes()
    {
        return id.clone();
    }

    public static IMinerID deserializeFromBytes(byte[] _bytes, IKiAPI _ki)
    {
        return MinerID.create(_bytes, _ki);
    }
}
