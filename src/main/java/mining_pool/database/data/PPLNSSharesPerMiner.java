package mining_pool.database.data;

import amp.Amplet;
import amp.classification.AmpClassCollection;
import amp.classification.classes.AC_SingleElement;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import com.ampex.amperabase.IKiAPI;

/**
 * Created by Queue on 2/18/2018.
 */
public class PPLNSSharesPerMiner implements IAmpAmpletSerializable
{
    private IMinerID minerID = null;
    private long shares = 0;

    private boolean valid = false;

    private PPLNSSharesPerMiner(IMinerID _minerID, long _shares)
    {
        if (_minerID != null)
        {
            minerID = _minerID;
            shares = _shares;
            valid = true;
        }
    }

    public static PPLNSSharesPerMiner create(IMinerID _minerID, long _shares)
    {
        PPLNSSharesPerMiner tempSharesPerMiner = new PPLNSSharesPerMiner(_minerID, _shares);
        if (tempSharesPerMiner.isValid())
        {
            return tempSharesPerMiner;
        }
        return null;
    }

    public IMinerID getMinerID()
    {
        return minerID;
    }

    public long getShares()
    {
        return shares;
    }

    public boolean isValid()
    {
        return valid;
    }

    private static final GroupID MINER_ID_GROUP_ID = new GroupID(1234, 55, "Miner id for pplns log");

    private static final GroupID SHARES_GROUP_ID = new GroupID(843, 76, "Shares of the miner");

    public Amplet serializeToAmplet()
    {
        AC_SingleElement minerIDAC = AC_SingleElement.create(MINER_ID_GROUP_ID, minerID);
        AC_SingleElement sharesAC = AC_SingleElement.create(SHARES_GROUP_ID, shares);

        AmpClassCollection sharesPerMinerCC = new AmpClassCollection();

        sharesPerMinerCC.addClass(minerIDAC);
        sharesPerMinerCC.addClass(sharesAC);

        return sharesPerMinerCC.serializeToAmplet();
    }

    public static PPLNSSharesPerMiner deserializeFromAmplet(Amplet _sharesPerMinerAmplet, IKiAPI _ki)
    {
        if (_sharesPerMinerAmplet == null)
        {
            return null;
        }

        UnpackedGroup minerIDGroup = _sharesPerMinerAmplet.unpackGroup(MINER_ID_GROUP_ID);
        UnpackedGroup sharesGroup = _sharesPerMinerAmplet.unpackGroup(SHARES_GROUP_ID);

        if (minerIDGroup == null || sharesGroup == null)
        {
            return null;
        }

        IMinerID minerID = MinerID.deserializeFromBytes(minerIDGroup.getElement(0), _ki);
        Long shares = sharesGroup.getElementAsLong(0);

        if (minerID == null || shares == null)
        {
            return null;
        }

        return PPLNSSharesPerMiner.create(minerID, shares);
    }
}
