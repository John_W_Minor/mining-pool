package mining_pool.database.data;

import amp.Amplet;
import amp.ByteTools;
import amp.classification.AmpClassCollection;
import amp.classification.classes.AC_ClassInstanceIDIsByteFootprint;
import amp.classification.classes.AC_SingleElement;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import amp.serialization.IAmpAmpletSerializable;
import com.ampex.amperabase.IKiAPI;

import java.util.ArrayList;

/**
 * Created by Queue on 2/18/2018.
 */
public class PPLNSRoundShareLog implements IAmpAmpletSerializable
{
    private ArrayList<PPLNSSharesPerMiner> sharesPerMinerObjects = new ArrayList<>();
    private ArrayList<BlockID> blockIDS = new ArrayList<>();
    private long roundEndTime = 0;
    private double poolFee = 0;


    private boolean valid = false;

    private PPLNSRoundShareLog(ArrayList<PPLNSSharesPerMiner> _sharesPerMinerObjects, ArrayList<BlockID> _blockIDS, long _roundEndTime, double _poolFee)
    {
        if (_sharesPerMinerObjects != null)
        {
            sharesPerMinerObjects.addAll(_sharesPerMinerObjects);
            blockIDS.addAll(_blockIDS);
            roundEndTime = _roundEndTime;
            poolFee=_poolFee;
            valid = true;
        }
    }

    public static PPLNSRoundShareLog create(ArrayList<PPLNSSharesPerMiner> _sharesPerMinerObjects, ArrayList<BlockID> _blockIDS, long _roundEndTime, double _poolFee)
    {
        PPLNSRoundShareLog tempLog = new PPLNSRoundShareLog(_sharesPerMinerObjects, _blockIDS, _roundEndTime, _poolFee);
        if (tempLog.isValid())
        {
            return tempLog;
        }
        return null;
    }

    public ArrayList<PPLNSSharesPerMiner> getSharesPerMinerObjects()
    {
        ArrayList<PPLNSSharesPerMiner> outgoing = new ArrayList<>();
        outgoing.addAll(sharesPerMinerObjects);

        return outgoing;
    }

    public ArrayList<BlockID> getBlockIDs()
    {
        ArrayList<BlockID> outgoing = new ArrayList<>();
        outgoing.addAll(blockIDS);
        return outgoing;
    }

    public long getRoundEndTime()
    {
        return roundEndTime;
    }

    public double getPoolFee()
    {
        return poolFee;
    }

    public boolean isValid()
    {
        return valid;
    }

    private static final long SHARES_PER_MINER_CLASS_ID = ByteTools.amplifyClassID(54421);

    private static final long BLOCK_IDS_CLASS_ID = ByteTools.amplifyClassID(9985);

    private static final GroupID ROUND_END_TIME_GROUP_ID = new GroupID(34525, 5532, "Round end time");

    private static final GroupID POOL_FEE_GROUP_ID = new GroupID(7524, 112121, "Pool fee");

    public Amplet serializeToAmplet()
    {
        AC_ClassInstanceIDIsByteFootprint sharesPerMinerAC = AC_ClassInstanceIDIsByteFootprint.create(SHARES_PER_MINER_CLASS_ID, "Shares per miner objects");

        for (PPLNSSharesPerMiner sharesPerMiner : sharesPerMinerObjects)
        {
            sharesPerMinerAC.addElement(sharesPerMiner);
        }

        AC_ClassInstanceIDIsByteFootprint blockIDsAC = AC_ClassInstanceIDIsByteFootprint.create(BLOCK_IDS_CLASS_ID, "block ids");

        for(BlockID blockID: blockIDS)
        {
            blockIDsAC.addElement(blockID);
        }

        AC_SingleElement roundEndTimeAC = AC_SingleElement.create(ROUND_END_TIME_GROUP_ID, roundEndTime);

        AC_SingleElement poolFeeAC = AC_SingleElement.create(POOL_FEE_GROUP_ID, poolFee);

        AmpClassCollection roundShareLogCC = new AmpClassCollection();

        roundShareLogCC.addClass(sharesPerMinerAC);
        roundShareLogCC.addClass(blockIDsAC);
        roundShareLogCC.addClass(roundEndTimeAC);
        roundShareLogCC.addClass(poolFeeAC);

        return roundShareLogCC.serializeToAmplet();
    }

    public static PPLNSRoundShareLog deserializeFromAmplet(Amplet _roundShareLogAmplet, IKiAPI _ki)
    {
        if (_roundShareLogAmplet == null)
        {
            return null;
        }

        UnpackedGroup roundEndTimeGroup = _roundShareLogAmplet.unpackGroup(ROUND_END_TIME_GROUP_ID);
        UnpackedGroup poolFeeGroup = _roundShareLogAmplet.unpackGroup(POOL_FEE_GROUP_ID);
        ArrayList<UnpackedGroup> sharesPerMinerClass = _roundShareLogAmplet.unpackClass(SHARES_PER_MINER_CLASS_ID);
        ArrayList<UnpackedGroup> blockIDsClass = _roundShareLogAmplet.unpackClass(BLOCK_IDS_CLASS_ID);

        if (sharesPerMinerClass == null || blockIDsClass == null || roundEndTimeGroup == null || poolFeeGroup == null)
        {
            return null;
        }

        Long roundEndTime = roundEndTimeGroup.getElementAsLong(0);
        Double poolFee = poolFeeGroup.getElementAsDouble(0);

        if (roundEndTime == null || poolFee == null)
        {
            return null;
        }

        ArrayList<PPLNSSharesPerMiner> sharesPerMinerObjects = new ArrayList<>();

        for (UnpackedGroup unpackedClassInstance : sharesPerMinerClass)
        {
            int count = unpackedClassInstance.getNumberOfElements();

            for (int i = 0; i < count; i++)
            {
                Amplet sharesPerMinerAmplet = unpackedClassInstance.getElementAsAmplet(i);

                if (sharesPerMinerAmplet == null)
                {
                    return null;
                }

                PPLNSSharesPerMiner tempSharesPerMiner = PPLNSSharesPerMiner.deserializeFromAmplet(sharesPerMinerAmplet, _ki);

                if (tempSharesPerMiner == null)
                {
                    return null;
                }

                sharesPerMinerObjects.add(tempSharesPerMiner);
            }
        }

        ArrayList<BlockID> blockIDs = new ArrayList<>();

        for (UnpackedGroup unpackedClassInstance : blockIDsClass)
        {
            int count = unpackedClassInstance.getNumberOfElements();

            for (int i = 0; i < count; i++)
            {
                Amplet blockIDAmplet = unpackedClassInstance.getElementAsAmplet(0);

                if(blockIDAmplet == null)
                {
                    return null;
                }

                BlockID tempBlockID = BlockID.deserializeFromAmplet(blockIDAmplet);

                if (tempBlockID == null)
                {
                    return null;
                }

                blockIDs.add(tempBlockID);
            }
        }

        return PPLNSRoundShareLog.create(sharesPerMinerObjects, blockIDs, roundEndTime, poolFee);
    }

    private static final long SHARES_LOGS_CLASS_ID = ByteTools.amplifyClassID(11215);

    public static Amplet serializeMultipleToAmplet(ArrayList<PPLNSRoundShareLog> _shareLogs)
    {
        if (_shareLogs == null || _shareLogs.size() == 0)
        {
            return null;
        }

        AC_ClassInstanceIDIsByteFootprint shareLogsAC = AC_ClassInstanceIDIsByteFootprint.create(SHARES_LOGS_CLASS_ID, "Share logs");

        for (PPLNSRoundShareLog shareLog : _shareLogs)
        {
            if (shareLog == null)
            {
                return null;
            }

            shareLogsAC.addElement(shareLog);
        }

        return shareLogsAC.serializeToAmplet();
    }

    public static ArrayList<PPLNSRoundShareLog> deserializeMultipleFromAmplet(Amplet _shareLogsAmplet, IKiAPI _ki)
    {
        if (_shareLogsAmplet == null)
        {
            return null;
        }

        ArrayList<UnpackedGroup> unpackedClass = _shareLogsAmplet.unpackClass(SHARES_LOGS_CLASS_ID);

        if (unpackedClass == null)
        {
            return null;
        }

        ArrayList<PPLNSRoundShareLog> shareLogs = new ArrayList<>();

        for (UnpackedGroup unpackedClassInstance : unpackedClass)
        {
            int count = unpackedClassInstance.getNumberOfElements();

            for (int i = 0; i < count; i++)
            {
                Amplet shareLogAmplet = unpackedClassInstance.getElementAsAmplet(i);

                if (shareLogAmplet == null)
                {
                    return null;
                }

                PPLNSRoundShareLog tempShareLog = PPLNSRoundShareLog.deserializeFromAmplet(shareLogAmplet, _ki);

                if (tempShareLog == null)
                {
                    return null;
                }

                shareLogs.add(tempShareLog);
            }
        }

        return shareLogs;
    }
}
