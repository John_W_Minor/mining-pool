package mining_pool.database.data;

import amp.AmpConstants;
import amp.Amplet;
import amp.ByteTools;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;

import java.util.ArrayList;

/**
 * Created by Queue on 1/15/2018.
 */
public class TimeRange implements ITimeRange
{
    private long range = 0;

    public TimeRange()
    {
        range = System.currentTimeMillis();
    }

    public TimeRange(long _range)
    {
        range = _range;
    }

    @Override
    public long getRange()
    {
        return range;
    }

    @Override
    public String getRangeAsString()
    {
        return Long.toString(range);
    }

    @Override
    public long getStartTime()
    {
        return range;
    }

    @Override
    public boolean isExpired(long _liveInterval)
    {
        long currentTime = System.currentTimeMillis() - _liveInterval;

        return currentTime > range;
    }

    private static final GroupID TIME_RANGE_GROUP_ID = new GroupID(6321,999, "Time Range");

    public static ArrayList<ITimeRange> deserializeTimeRanges(Amplet _timeRangesAmplet)
    {
        if (_timeRangesAmplet != null)
        {
            UnpackedGroup timeRangesGroup = _timeRangesAmplet.unpackGroup(TIME_RANGE_GROUP_ID);

            if (timeRangesGroup != null && timeRangesGroup.getHeader().getByteCountPerElement() == AmpConstants.LONG_BYTE_FOOTPRINT)
            {
                int count = timeRangesGroup.getNumberOfElements();

                if (count != 0)
                {
                    ArrayList<ITimeRange> timeRanges = new ArrayList<>();

                    for (int i = 0; i < count; i++)
                    {
                        timeRanges.add(new TimeRange(timeRangesGroup.getElementAsLong(i)));
                    }

                    return timeRanges;
                }
            }
        }
        return null;
    }

    public static Amplet serializeTimeRanges(ArrayList<ITimeRange> _timeRanges)
    {
        if(_timeRanges != null)
        {
            UnpackedGroup timeRangesGroup = UnpackedGroup.create(TIME_RANGE_GROUP_ID, AmpConstants.LONG_BYTE_FOOTPRINT);

            for (ITimeRange timeRange : _timeRanges)
            {
                byte[] bytes = ByteTools.deconstructLong(timeRange.getRange());
                timeRangesGroup.addElement(bytes);
            }

            return timeRangesGroup.serializeToAmplet();
        }

        return null;
    }
}
