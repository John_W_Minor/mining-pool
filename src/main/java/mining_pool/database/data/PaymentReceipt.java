package mining_pool.database.data;

import amp.Amplet;
import amp.classification.AmpClassCollection;
import amp.classification.classes.AC_SingleElement;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import com.ampex.amperabase.IKiAPI;

/**
 * Created by Queue on 1/24/2018.
 */
public class PaymentReceipt implements IPaymentReceipt
{
    private IMinerID payee = null;
    private long originToPay = 0;
    private boolean validPaymentReceipt = false;

    private PaymentReceipt(IMinerID _payee, long _originToPay)
    {
        if (_payee != null && _originToPay > 0)
        {
            payee = _payee;
            originToPay = _originToPay;
            validPaymentReceipt = true;
        }
    }

    public static PaymentReceipt create(IMinerID _payee, long _originToPay)
    {
        PaymentReceipt tempPaymentReceipt = new PaymentReceipt(_payee, _originToPay);
        if (tempPaymentReceipt.isValidPaymentReceipt())
        {
            return tempPaymentReceipt;
        }

        return null;
    }

    @Override
    public IMinerID getPayee()
    {
        return payee;
    }

    @Override
    public long getOriginToPay()
    {
        return originToPay;
    }

    @Override
    public boolean isValidPaymentReceipt()
    {
        return validPaymentReceipt;
    }

    private static final GroupID PAYEE_GROUP_ID = new GroupID(47,2, "Payee");

    private static final GroupID ORIGIN_TO_PAY_GROUP_ID = new GroupID(54,888, "Origin To Pay");

    @Override
    public Amplet serializeToAmplet()
    {
        AC_SingleElement payeeAC = AC_SingleElement.create(PAYEE_GROUP_ID, payee);

        AC_SingleElement originToPayAC = AC_SingleElement.create(ORIGIN_TO_PAY_GROUP_ID, originToPay);

        AmpClassCollection paymentReceiptCC = new AmpClassCollection();

        paymentReceiptCC.addClass(payeeAC);
        paymentReceiptCC.addClass(originToPayAC);

        return paymentReceiptCC.serializeToAmplet();
    }

    public static IPaymentReceipt deserializeFromAmplet(Amplet _paymentReceiptAmplet, IKiAPI _ki)
    {
        if (_paymentReceiptAmplet != null)
        {
            UnpackedGroup payeeGroup = _paymentReceiptAmplet.unpackGroup(PAYEE_GROUP_ID);
            UnpackedGroup originToPayGroup = _paymentReceiptAmplet.unpackGroup(ORIGIN_TO_PAY_GROUP_ID);

            if (payeeGroup != null && originToPayGroup != null)
            {
                byte[] minerIDBytes = payeeGroup.getElement(0);
                IMinerID payee = MinerID.create(minerIDBytes, _ki);

                Long originToPay = originToPayGroup.getElementAsLong(0);

                if (payee != null && originToPay != null)
                {
                    return PaymentReceipt.create(payee, originToPay);
                }
            }
        }

        return null;
    }
}
