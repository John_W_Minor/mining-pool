package mining_pool.database;

import mining_pool.verification.data.VerifiedShare;

import java.util.ArrayList;

/**
 * Created by Queue on 2/17/2018.
 */
public class PPLNSMemoryDatabase
{
    private volatile ArrayList<VerifiedShare> lastNShares = new ArrayList<>();
    private volatile long n = 1000;

    public synchronized void addPPLNSShare(VerifiedShare _pplnsShare)
    {
        if (_pplnsShare == null)
        {
            return;
        }

        lastNShares.add(_pplnsShare);

        while (lastNShares.size() > n)
        {
            lastNShares.remove(0);
        }
    }

    public synchronized ArrayList<VerifiedShare> getLastNShares()
    {
        ArrayList<VerifiedShare> outgoing = new ArrayList<>();
        outgoing.addAll(lastNShares);

        return outgoing;
    }

    public synchronized void setN(long _n)
    {
        if (_n < 1)
        {
            _n = 1;
        }

        n = _n;

        while (lastNShares.size() > n)
        {
            lastNShares.remove(0);
        }
    }

    public synchronized long getN()
    {
        return n;
    }
}
