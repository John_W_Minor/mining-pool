package mining_pool.database;


import database.XodusAmpMap;

/**
 * Created by Queue on 1/20/2018.
 */
public class DBConnection
{
    private String location = null;

    private XodusAmpMap xam = null;

    public DBConnection(String _location)
    {
        location = _location;

        xam = new XodusAmpMap(location);
    }

    public String getLocation()
    {
        return location;
    }

    public XodusAmpMap getXAM()
    {
        return xam;
    }
}
