package mining_pool.database;

import amp.AmpConstants;
import amp.ByteTools;
import amp.Amplet;
import amp.group_ids.GroupID;
import amp.group_primitives.UnpackedGroup;
import com.ampex.amperabase.IKiAPI;
import database.XodusAmpMap;
import logging.IAmpexLogger;
import mining_pool.PoolLogging;
import mining_pool.database.data.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Queue on 1/15/2018.
 */
public class Database
{
    private static final String ROOT = "poolDB";
    private static final String META = "/metaDB";
    private static final String SHARES = "/sharesDB";
    private static final String RECEIPTS = "/receiptsDB";
    private static final String PPLNSROUNDS = "/pplnsRoundDB";

    private List<DBConnection> dbConns = Collections.synchronizedList(new ArrayList<DBConnection>());

    private IKiAPI ki;

    public Database(IKiAPI _ki)
    {
        ki=_ki;
    }

    //methods for interacting with the metaDB

    //time ranges
    private static final String META_TIME_RANGE_KEY = "time range";

    public synchronized ArrayList<ITimeRange> getTimeRanges() throws Exception
    {
        XodusAmpMap xam = connectToMetaDatabase();

        Amplet timeRangesAmplet = xam.get(META_TIME_RANGE_KEY);

        return TimeRange.deserializeTimeRanges(timeRangesAmplet);
    }

    public synchronized void putTimeRanges(ArrayList<ITimeRange> _timeRanges)
    {
        XodusAmpMap xam = connectToMetaDatabase();

        Amplet timeRangesAmplet = TimeRange.serializeTimeRanges(_timeRanges);

        xam.put(META_TIME_RANGE_KEY.getBytes(AmpConstants.UTF8), timeRangesAmplet);
    }

    //miner ids
    private static final String META_MINER_INDEX_IDS_KEY = "miner index ids";

    private static final GroupID MINER_INDEX_GROUP_ID = new GroupID(444, 321, "Miner Index");

    public synchronized ArrayList<Integer> getIndexIDs(ITimeRange _timeRange) throws Exception
    {
        XodusAmpMap xam = connectToMetaDatabase(_timeRange.getRangeAsString());

        Amplet indexIDsAmplet = xam.get(META_MINER_INDEX_IDS_KEY);

        if (indexIDsAmplet != null)
        {
            UnpackedGroup indexIDsGroup = indexIDsAmplet.unpackGroup(MINER_INDEX_GROUP_ID);

            if (indexIDsGroup != null)
            {
                int indexIDsCount = indexIDsGroup.getNumberOfElements();

                ArrayList<Integer> indexIDs = new ArrayList<>();

                for (int i = 0; i < indexIDsCount; i++)
                {
                    indexIDs.add(indexIDsGroup.getElementAsInt(i));
                }

                return indexIDs;
            }
        }

        return null;
    }

    private synchronized boolean putIndexID(int _indexID, ITimeRange _timeRange) throws Exception
    {
        boolean success = false;

        if (_timeRange != null)
        {
            ArrayList<Integer> indexIDs = getIndexIDs(_timeRange);

            if (indexIDs != null)
            {
                for (int indexID : indexIDs)
                {
                    if (indexID == _indexID)
                    {
                        return true;
                    }
                }

            } else
            {
                indexIDs = new ArrayList<>();
            }

            indexIDs.add(_indexID);

            UnpackedGroup indexIDsGroup = UnpackedGroup.create(MINER_INDEX_GROUP_ID, AmpConstants.INT_BYTE_FOOTPRINT);

            for (int indexID : indexIDs)
            {
                byte[] bytes = ByteTools.deconstructInt(indexID);
                indexIDsGroup.addElement(bytes);
            }

            XodusAmpMap xam = connectToMetaDatabase(_timeRange.getRangeAsString());

            success = xam.put(META_MINER_INDEX_IDS_KEY, indexIDsGroup);
        }

        return success;
    }

    private synchronized boolean removeIndexIDIfUseless(int _indexID, ITimeRange _timeRange) throws Exception
    {
        boolean success = true;

        if (_timeRange != null)
        {
            MinerIndex tempMinerIndex = getMinerIndex(_indexID, _timeRange);

            if (tempMinerIndex == null)
            {

                ArrayList<Integer> indexIDs = getIndexIDs(_timeRange);

                if (indexIDs != null)
                {
                    int count = indexIDs.size();

                    for (int i = 0; i < count; i++)
                    {
                        if (_indexID == indexIDs.get(i))
                        {
                            indexIDs.remove(i);
                            break;
                        }
                    }

                    UnpackedGroup indexIDsGroup = UnpackedGroup.create(MINER_INDEX_GROUP_ID, AmpConstants.INT_BYTE_FOOTPRINT);

                    for (int indexID : indexIDs)
                    {
                        indexIDsGroup.addElement(ByteTools.deconstructInt(indexID));
                    }

                    XodusAmpMap xam = connectToMetaDatabase(_timeRange.getRangeAsString());

                    success = xam.put(META_MINER_INDEX_IDS_KEY, indexIDsGroup);
                }
            }
        }

        return success;
    }

    private static final String META_MINER_SINGLE_INDEX_KEY = "miner index: ";

    public synchronized ArrayList<MinerIndex> getMinerIndices(ITimeRange _timeRange) throws Exception
    {
        ArrayList<Integer> indexIDs = getIndexIDs(_timeRange);

        if (indexIDs != null)
        {
            ArrayList<MinerIndex> minerIndices = new ArrayList<>();

            for (int indexID : indexIDs)
            {
                MinerIndex tempMinerIndex = getMinerIndex(indexID, _timeRange);

                if (tempMinerIndex != null)
                {
                    minerIndices.add(tempMinerIndex);
                } else
                {
                    throw new Exception();
                    //return null;
                }
            }

            return minerIndices;
        }
        return null;
    }

    public synchronized MinerIndex getMinerIndex(int _indexID, ITimeRange _timeRange) throws Exception
    {
        if (_timeRange != null)
        {
            XodusAmpMap xam = connectToMetaDatabase(_timeRange.getRangeAsString());

            byte[] minerIndexBytes = xam.getBytes(META_MINER_SINGLE_INDEX_KEY + _indexID);

            if (minerIndexBytes != null)
            {
                return MinerIndex.deserializeFromBytes(minerIndexBytes, _indexID, _timeRange, ki);
            }
        }
        return null;
    }

    public synchronized boolean putMinerIndex(MinerIndex _minerIndex) throws Exception
    {
        boolean success = false;

        if (_minerIndex != null)
        {
            int indexID = _minerIndex.getId();

            ITimeRange timeRange = _minerIndex.getTimeRange();

            success = putIndexID(indexID, timeRange);

            if (success)
            {
                XodusAmpMap xam = connectToMetaDatabase(timeRange.getRangeAsString());

                success = xam.putBytes(META_MINER_SINGLE_INDEX_KEY + indexID, _minerIndex.serializeToBytes());

                if (!success)
                {
                    if (!removeIndexIDIfUseless(indexID, timeRange))
                    {
                        throw new Exception();
                    }
                }
            }
        }

        return success;
    }

    public synchronized boolean addNewMinerID(IMinerID _minerID, ITimeRange _timeRange) throws Exception
    {
        if (_minerID != null && _timeRange != null)
        {
            ArrayList<Integer> indexIDs = getIndexIDs(_timeRange);
            int lastIndexID = -1;
            if (indexIDs != null)
            {
                int indexIDsCount = indexIDs.size();
                lastIndexID = indexIDs.get(indexIDsCount - 1);
            }

            MinerIndex lastMinerIndex = getMinerIndex(lastIndexID, _timeRange);

            if (lastMinerIndex != null && !lastMinerIndex.isFull())
            {
                lastMinerIndex.addElement(_minerID);

                return putMinerIndex(lastMinerIndex);
            } else
            {
                int newIndexID = lastIndexID + 1;

                MinerIndex newMinerIndex = MinerIndex.create(newIndexID, _timeRange);

                if (newMinerIndex != null)
                {
                    newMinerIndex.addElement(_minerID);

                    return putMinerIndex(newMinerIndex);
                }
            }
        }

        return false;
    }

    //methods for interacting with the sharesDB
    private synchronized int getShareSelector(IMinerID _minerID)
    {
        byte[] key = _minerID.serializeToBytes();
        if(key.length > 5)
        {
            int selector = ByteTools.buildInt(key[key.length - 4], key[key.length - 3], key[key.length - 2], key[key.length - 1]);
            selector %= 1000;
            return selector;
        }
        return 0;
    }

    public synchronized Long getTotalSharesOfMiner(IMinerID _minerID, ITimeRange _timeRange)
    {
        if (_minerID != null && _timeRange != null)
        {
            byte[] key = _minerID.serializeToBytes();

            int selector = getShareSelector(_minerID);

            XodusAmpMap xam = connectToSharesDatabase(_timeRange.getRangeAsString() + "/" + selector);

            byte[] bytes = xam.getBytes(key);

            if (bytes != null && bytes.length == AmpConstants.LONG_BYTE_FOOTPRINT)
            {
                Long value = ByteTools.buildLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);

                //Logger logger = PoolLogging.getLogger();
                //if(logger != null)
                {
                    //logger.debug("Getting total shares of a given miner succeeded, was" + value.toString() + " shares. This was the miner ID: " + _minerID.getIDAsString());
                }

                return value;
            }

            IAmpexLogger logger = PoolLogging.getLogger();
            if(logger != null)
            {
                logger.debug("Getting total shares of a given miner failed. This was the miner ID: " + _minerID.getIDAsString());
            }
        }

        return null;
    }

    public synchronized boolean incrementTotalSharesOfMiner(IMinerID _minerID, ITimeRange _timeRange) throws Exception
    {
        if (_minerID != null && _timeRange != null)
        {
            byte[] key = _minerID.serializeToBytes();

            int selector = getShareSelector(_minerID);

            Long lastValue = getTotalSharesOfMiner(_minerID, _timeRange);
            if (lastValue == null)
            {
                addNewMinerID(_minerID, _timeRange);
                lastValue = 0L;
            }

            long newValue = lastValue + 1;

            byte[] bytes = ByteTools.deconstructLong(newValue);

            XodusAmpMap xam = connectToSharesDatabase(_timeRange.getRangeAsString() + "/" + selector);

            //Logger logger = PoolLogging.getLogger();
            //if(logger != null)
            {
                //logger.debug("Incrementing total shares of a given miner, putting " + newValue + " shares. This was the miner ID: " + _minerID.getIDAsString());
            }

            return xam.putBytes(key, bytes);
        }
        return false;
    }

    private static final String PAY_PERIOD_SHARES_KEY = "pay period shares key";

    public synchronized Long getTotalSharesOfPayPeriod(ITimeRange _timeRange)
    {
        if (_timeRange != null)
        {
            XodusAmpMap xam = connectToSharesDatabase(_timeRange.getRangeAsString());

            byte[] bytes = xam.getBytes(PAY_PERIOD_SHARES_KEY);

            if (bytes != null && bytes.length == AmpConstants.LONG_BYTE_FOOTPRINT)
            {
                Long value = ByteTools.buildLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);

                //Logger logger = PoolLogging.getLogger();
                //if(logger != null)
                {
                    //logger.debug("Getting total shares of payperiod, was" + value.toString() + " shares.");
                }

                return value;
            }

            //System.out.println("Getting total shares of pay period failed.");
        }

        return 0L;
    }

    public synchronized boolean incrementTotalSharesOfPayPeriod(ITimeRange _timeRange)
    {
        if (_timeRange != null)
        {
            Long lastValue = getTotalSharesOfPayPeriod(_timeRange);
            if (lastValue == null)
            {
                lastValue = 0L;
            }

            long newValue = lastValue + 1;

            byte[] bytes = ByteTools.deconstructLong(newValue);

            XodusAmpMap xam = connectToSharesDatabase(_timeRange.getRangeAsString());

            //Logger logger = PoolLogging.getLogger();
            //if(logger != null)
            {
                //logger.debug("Incrementing total shares of pay period, putting " + newValue + " shares.");
            }

            return xam.putBytes(PAY_PERIOD_SHARES_KEY, bytes);
        }
        return false;
    }

    //methods for interacting with the receiptsDB

    private static final String CURRENT_RECEIPT_KEY = "current receipt";
    private static final String RECEIPT_KEY = "receipt: ";

    public synchronized long getCurrentReceiptIndex()
    {
        XodusAmpMap xam = connectToReceiptsDatabase();

        long currentReceiptIndex = -1;

        byte[] bytes = xam.getBytes(CURRENT_RECEIPT_KEY);

        if (bytes != null && bytes.length == AmpConstants.LONG_BYTE_FOOTPRINT)
        {
            currentReceiptIndex = ByteTools.buildLong(bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]);
        }
//        System.out.println("Index: " + currentReceiptIndex);
        return currentReceiptIndex;
    }

    private synchronized boolean setCurrentReceiptIndex(long _receiptIndex)
    {
        XodusAmpMap xam = connectToReceiptsDatabase();

        byte[] bytes = ByteTools.deconstructLong(_receiptIndex);

        return xam.putBytes(CURRENT_RECEIPT_KEY, bytes);
    }

    public synchronized boolean pushPaymentReceipt(IPaymentReceipt _receipt)
    {
        boolean success = false;

        if (_receipt != null)
        {
            long currentReceiptIndex = getCurrentReceiptIndex();

            currentReceiptIndex++;

            XodusAmpMap xam = connectToReceiptsDatabase();

            success = xam.put(RECEIPT_KEY + currentReceiptIndex, _receipt);

            if (success)
            {
                success = setCurrentReceiptIndex(currentReceiptIndex);
            }
//            if(success)
//                System.out.println("Successfully pushed payment receipt. New index: " + currentReceiptIndex);
        }

        return success;
    }

    public synchronized IPaymentReceipt popPaymentReceipt() throws Exception
    {
        long currentReceiptIndex = getCurrentReceiptIndex();

        if (currentReceiptIndex != -1)
        {
            XodusAmpMap xam = connectToReceiptsDatabase();

            String key = RECEIPT_KEY + currentReceiptIndex;

            Amplet paymentReceiptAmp = xam.get(key);

            currentReceiptIndex--;
            setCurrentReceiptIndex(currentReceiptIndex);

            xam.remove(key);

            return PaymentReceipt.deserializeFromAmplet(paymentReceiptAmp, ki);
        }

        return null;
    }

    public synchronized IPaymentReceipt peekPaymentReceipt() throws Exception
    {
        long currentReceiptIndex = getCurrentReceiptIndex();

        if (currentReceiptIndex != -1)
        {
            XodusAmpMap xam = connectToReceiptsDatabase();

            String key = RECEIPT_KEY + currentReceiptIndex;

            Amplet paymentReceiptAmp = xam.get(key);

            return PaymentReceipt.deserializeFromAmplet(paymentReceiptAmp, ki);
        }

        return null;
    }

    public synchronized IPaymentReceipt getPaymentReceiptByIndex(long _index) throws Exception
    {
        XodusAmpMap xam = connectToReceiptsDatabase();

        String key = RECEIPT_KEY + _index;

        Amplet paymentReceiptAmp = xam.get(key);

        if (paymentReceiptAmp != null)
        {
            return PaymentReceipt.deserializeFromAmplet(paymentReceiptAmp, ki);
        }

        return null;
    }

    //methods for interacting with the pplns rounds db

    private static final String ROUND_SHARE_LOGS_KEY = "round share logs";

    public synchronized ArrayList<PPLNSRoundShareLog> getRoundShareLogs() throws Exception
    {
        XodusAmpMap xam = connectToPPLNSRoundsDatabase();

        Amplet roundShareLogsAmplet = xam.get(ROUND_SHARE_LOGS_KEY);

        ArrayList<PPLNSRoundShareLog> logs = PPLNSRoundShareLog.deserializeMultipleFromAmplet(roundShareLogsAmplet, ki);

        if(logs == null)
        {
            logs = new ArrayList<>();
        }

        return logs;
    }

    public synchronized boolean putRoundShareLogs(ArrayList<PPLNSRoundShareLog> _roundShareLogs)
    {
        if(_roundShareLogs == null)
        {
            return false;
        }

        XodusAmpMap xam = connectToPPLNSRoundsDatabase();

        Amplet roundShareLogsAmplet = PPLNSRoundShareLog.serializeMultipleToAmplet(_roundShareLogs);

        if (roundShareLogsAmplet == null)
        {
            xam.remove(ROUND_SHARE_LOGS_KEY);
            return true;
        }

        return xam.put(ROUND_SHARE_LOGS_KEY, roundShareLogsAmplet);
    }

    //methods for connecting to the db
    private synchronized XodusAmpMap connectToMetaDatabase()
    {
        return connectToDatabase(ROOT + META);
    }

    private synchronized XodusAmpMap connectToMetaDatabase(String _subDatabase)
    {
        return connectToDatabase(ROOT + META + "/" + _subDatabase);
    }

    private synchronized XodusAmpMap connectToSharesDatabase()
    {
        return connectToDatabase(ROOT + SHARES);
    }

    private synchronized XodusAmpMap connectToSharesDatabase(String _subDatabase)
    {
        return connectToDatabase(ROOT + SHARES + "/" + _subDatabase);
    }

    public synchronized XodusAmpMap connectToReceiptsDatabase()
    {
        return connectToDatabase(ROOT + RECEIPTS);
    }

    public synchronized XodusAmpMap connectToPPLNSRoundsDatabase()
    {
        return connectToDatabase(ROOT + PPLNSROUNDS);
    }

    private synchronized XodusAmpMap connectToDatabase(String _location)
    {
        for (DBConnection dbConn : dbConns)
        {
            if (dbConn.getLocation().equals(_location))
            {
                return dbConn.getXAM();
            }
        }

        DBConnection tempDBConn = new DBConnection(_location);

        dbConns.add(tempDBConn);

        return tempDBConn.getXAM();
    }

    public synchronized void resetConnections()
    {
        for (DBConnection dbConn : dbConns)
        {
            dbConn.getXAM().close();
        }

        dbConns.clear();
    }

    //methods for deleting databases
    public synchronized boolean deleteMetaDatabase()
    {
        return deleteDatabase(ROOT + META);
    }

    public synchronized boolean deleteMetaDatabase(String _subDatabase)
    {
        return deleteDatabase(ROOT + META + "/" + _subDatabase);
    }

    public synchronized boolean deleteSharesDatabase()
    {
        return deleteDatabase(ROOT + SHARES);
    }

    public synchronized boolean deleteSharesDatabase(String _subDatabase)
    {
        return deleteDatabase(ROOT + SHARES + "/" + _subDatabase);
    }

    public synchronized boolean deleteReceiptsDatabase()
    {
        return deleteDatabase(ROOT + RECEIPTS);
    }

    public synchronized boolean deletePPLNSRoundsDatabase()
    {
        return deleteDatabase(ROOT + PPLNSROUNDS);
    }

    public synchronized boolean nukeDatabase()
    {
        return deleteDatabase(ROOT);
    }

    private synchronized boolean deleteDatabase(String _location)
    {
        Iterator<DBConnection> dbConnIter = dbConns.iterator();

        while (dbConnIter.hasNext())
        {
            DBConnection dbConn = dbConnIter.next();
            if (dbConn.getLocation().toLowerCase().contains(_location.toLowerCase()))
            {
                dbConn.getXAM().close();
                dbConnIter.remove();
            }
        }

        return deleteLocation(new File(_location));
    }

    private synchronized boolean deleteLocation(File _location)
    {
        if (_location.isDirectory())
        {
            File[] children = _location.listFiles();
            if (children != null)
            {
                for (File child : children)
                {
                    boolean success = deleteLocation(child);
                    if (!success)
                    {
                        return false;
                    }
                }
            }
        }

        return _location.delete();
    }

    //misc
    public synchronized void gc()
    {
        for (DBConnection dbConn : dbConns)
        {
            dbConn.getXAM().resumeGC();
        }

        try
        {
            Thread.sleep(10);
        } catch (Exception e)
        {

        }
    }
}
