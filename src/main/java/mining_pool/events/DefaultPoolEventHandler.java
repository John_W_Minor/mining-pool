package mining_pool.events;

import logging.IAmpexLogger;
import mining_pool.IPool;
import mining_pool.PoolLogging;

public class DefaultPoolEventHandler implements IPoolEventHandler
{
    @Override
    public void handle(IPool _pool, PoolEventType _type) throws Exception
    {
        IAmpexLogger logger = PoolLogging.getLogger();
        if(logger != null)
        {
            switch (_type)
            {
                case NEW_HEIGHT:
                    logger.debug("Height has changed.");
                    break;

                case NEW_SHARE_DIFFICULTY:
                    logger.debug("Share difficulty has changed.");
                    break;

                case NEW_PAY_PER_SHARE:
                    logger.debug("Pay per share has changed.");
                    break;

                case RECEIPTS_AVAILABLE:
                    logger.debug("New payment receipts are available for processing.");
                    break;

                case SHARE_VERIFICATION_REPORT_AVAILABLE:
                    logger.debug("New share verification report available.");
                    break;

                case NEW_PAY_INTERVAL:
                    logger.debug("Pay interval has changed.");
                    break;
            }
        }
    }
}
