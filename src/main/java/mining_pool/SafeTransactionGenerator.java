package mining_pool;

import com.ampex.amperabase.*;
import logging.IAmpexLogger;
import mining_pool.database.data.IMinerID;
import mining_pool.database.data.IPaymentReceipt;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Queue on 2/6/2018.
 */
public class SafeTransactionGenerator
{
    private static final long MILLISECONDS_IN_SECOND = 1000;
    private static final long MILLISECONDS_IN_MINUTE = 60 * MILLISECONDS_IN_SECOND;
    private static final long MILLISECONDS_IN_HOUR = 60 * MILLISECONDS_IN_MINUTE;

    private IPool pool;
    private IKiAPI ki;

    public SafeTransactionGenerator(IPool _pool, IKiAPI _ki)
    {
        pool = _pool;
        ki = _ki;
    }

    public synchronized ITransAPI generatePayPeriodTransaction(String _message)
    {
        if (!pool.areReceiptsAvailable() || pool.peekReceipt() == null)
        {
            return null;
        }

        List<IOutput> outputs = new ArrayList<>();

        BigInteger funds = ki.getTransMan().getAmountInWallet(ki.getAddMan().getMainAdd(), Token.ORIGIN);

        BigInteger feeBuffer = BigInteger.valueOf(50);

        BigInteger total = BigInteger.valueOf(150);

        int index = 0;
        while (outputs.size()<10_000)
        {
            IPaymentReceipt peekedReceipt = pool.peekReceipt();

            if(peekedReceipt == null)
            {
                break;
            }

            IOutput tempOutput = getOutputs(peekedReceipt, index);
            index++;

            if(tempOutput == null)
            {
                pool.popReceipt();
                continue;
            }

            BigInteger originToPay = BigInteger.valueOf(peekedReceipt.getOriginToPay());

            BigInteger tempTotal = total.add(feeBuffer).add(originToPay);

            if(tempTotal.compareTo(funds) > 0)
            {
                break;
            }

            total=tempTotal;

            outputs.add(tempOutput);

            pool.popReceipt();
        }

        List<IInput> inputs = getInputs(total);

        return finalizeTransaction(outputs, inputs, _message);
    }

    private synchronized IOutput getOutputs(IPaymentReceipt _receipt, int _index)
    {
        IMinerID peekedMinerID = _receipt.getPayee();

        String peekedAddressString = peekedMinerID.getIDAsString();

        IAddress receiptAddress = ki.getAddMan().decodeFromChain(peekedAddressString);

        BigInteger originToPay = BigInteger.valueOf(_receipt.getOriginToPay());

        if(receiptAddress.isValid())
        {
            return ki.getTransMan().createOutput(originToPay, receiptAddress, Token.ORIGIN, _index, System.currentTimeMillis());
        }

        return null;
    }

    private synchronized List<IInput> getInputs(BigInteger _total)
    {
        List<IInput> inputs = ki.getTransMan().getInputsForAmountAndToken(ki.getAddMan().getMainAdd(), _total, Token.ORIGIN, true);

        if (inputs == null)
        {
            return null;
        }

        return inputs;
    }

    private synchronized ITransAPI finalizeTransaction(List<IOutput> _outputs, List<IInput> _inputs, String _message)
    {
        if (_inputs.isEmpty() || _outputs.isEmpty())
        {
            return null;
        }

        ArrayList<String> stringInputs = new ArrayList<>();
        for (IInput input : _inputs)
        {
            stringInputs.add(input.getID());
        }
        IKSEP ksep = ki.getTransMan().createKSEP(null, ki.getAddMan().getEntropyForAdd(ki.getAddMan().getMainAdd()), stringInputs, ki.getAddMan().getMainAdd().getPrefix(), false, ki.getAddMan().getMainAdd().getKeyType());

        Map<String, IKSEP> keySigMap = new HashMap<>();

        keySigMap.put(ki.getEncryptMan().getPublicKeyString(ki.getAddMan().getMainAdd().getKeyType()), ksep);

        ITransAPI tempTransaction = null;

        try
        {
            tempTransaction = ki.getTransMan().createTrans(_message,_outputs,_inputs,keySigMap);
            tempTransaction.makeChange(TransactionFeeCalculator.calculateMinFee(tempTransaction).add(BigInteger.valueOf(50)), ki.getAddMan().getMainAdd());
            tempTransaction.addSig(ki.getEncryptMan().getPublicKeyString(ki.getAddMan().getMainAdd().getKeyType()), Utils.toBase64(ki.getEncryptMan().sign(tempTransaction.toSignBytes(), ki.getAddMan().getMainAdd().getKeyType())));
        } catch (Exception e)
        {
            IAmpexLogger logger = PoolLogging.getLogger();
            if (logger != null)
            {
                logger.error("", e);
            }
        }

        return tempTransaction;
    }
}
